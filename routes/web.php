<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::post('/password_reset/sendmail','Auth\AuthController@postSenMail')->name('auth.reset.password');
Route::get('reset-password/{token}', [
    'uses' => 'Auth\AuthController@resetPassword',
    'as' => 'auth.reset.resetpassword',
]);
Route::post('update-password', [
    'uses' => 'Auth\AuthController@updatePassword',
    'as' => 'auth.reset.updatepassword',
]);
Route::get('/home', 'HomeController@index')->name('home')->middleware('roleauth:admin|editor');
//Route::get('/home/vue', 'HomeController@vue')->name('home.vue');


Route::namespace('Admin')->middleware('roleauth:admin|editor')->prefix('admin')->group(function () {
	Route::get('/', [
        'uses' => 'IndexController@index',
        'as' => 'admin.index.index'
    ]);
	Route::get('/post/search', 'PostIndexController@search')->name('post.search');
	Route::get('/post/status', 'PostIndexController@status')->name('post.status');
	Route::get('/post/searchTags', 'PostIndexController@searchTags')->name('post.searchTags');
	Route::resource('post', 'PostIndexController');
    
    Route::get('/cat/search', 'CategoryController@search')->name('cat.search')->middleware('roleauth:admin');
    Route::resource('cat', 'CategoryController')->middleware('roleauth:admin');    
    Route::get('/tags/search', 'TagsController@search')->name('tags.search')->middleware('roleauth:admin');
    Route::resource('tags', 'TagsController')->middleware('roleauth:admin');

	Route::resource('groups', 'GroupsIndexController');
    Route::get('/profile', 'UserIndexController@profile')->name('profile');
    Route::get('/message', 'UserIndexController@message')->name('message');
    Route::post('/message', 'UserIndexController@postMessage')->name('user.postmessage');
    Route::post('/admin/users/profile/{id}', [
        'uses' => 'UserIndexController@postAddProfile',
        'as' => 'user.profile'
    ]);
    Route::post('/admin/users/change-avatar', [
        'uses' => 'UserIndexController@changeAvatar',
        'as' => 'user.avatar'
    ]);
    Route::resource('user', 'UserIndexController');


});

Route::get('notify', 'SendMessageController@index')->name('index');
Route::get('notify/send', 'SendMessageController@send')->name('send');
Route::post('notify/postMessage', 'SendMessageController@sendMessage')->name('postMessage');

Route::namespace('Vpublic')->group(function () {

    Route::get('/chat', 'ChatsController@index');
    Route::get('/chat/messages', 'ChatsController@fetchMessages');
    Route::post('/chat/messages', 'ChatsController@sendMessage');



    Route::get('/', [
        'uses' => 'PcIndexController@index',
        'as' => 'public.index'
    ]);
    Route::get('/search', [
        'uses' => 'PcIndexController@search',
        'as' => 'public.search'
    ]);
    Route::post('/review', [
        'uses' => 'PcIndexController@postReview',
        'as' => 'public.review'
    ])->middleware('auth');
    Route::get('/{cat}/{title}_{id}.html', [
        'uses' => 'PcIndexController@detail',
        'as' => 'public.detail'
    ]);
    Route::get('/{cat}_{id}', [
        'uses' => 'PcIndexController@category',
        'as' => 'public.category'
    ]);



});
Route::get('email', 'EmailController@sendEmail');