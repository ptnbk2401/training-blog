
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Vue from 'vue'
import Router from 'vue-router'
import VueSelect from 'vue-select'
import DataTable from 'v-data-table'

import MainComponent from './components/MainComponent'
import IndexComponent from './components/user/IndexComponent'
import EditComponent from './components/user/EditComponent'
import CreateComponent from './components/user/CreateComponent'
import MyVuetable from './components/user/MyVuetable'
// import Datatabledemo from './components/user/Datatable'

import GIndexComponent from './components/groups/IndexComponent'
import GEditComponent from './components/groups/EditComponent'
import GCreateComponent from './components/groups/CreateComponent'

import ChatMessages from './components/chat/ChatMessages.vue'
import ChatForm from './components/chat/ChatForm.vue'


Vue.use(DataTable);
Vue.use(Router);
Vue.component('v-select', VueSelect);
Vue.component('my-vuetable', MyVuetable);
Vue.component('chat-messages', ChatMessages);
Vue.component('chat-form', ChatForm);


export const router = new Router({
	mode: 'history',
    routes: [
        {
            path: '/admin/user',
            name: 'user.index',
            component: IndexComponent
        },
        {
            path: '/admin/user/:id/edit',
            name: 'user.edit',
            component: EditComponent,
        },

        {
            path: '/admin/user/create',
            name: 'user.create',
            component: CreateComponent,
        },
        {
            path: '/admin/groups',
            name: 'groups.index',
            component: GIndexComponent
        },
        {
            path: '/admin/groups/:id/edit',
            name: 'groups.edit',
            component: GEditComponent,
        },

        {
            path: '/admin/groups/create',
            name: 'groups.create',
            component: GCreateComponent,
        },

    ],
})

const app = new Vue({
    el: '#app',
    components: { MainComponent },
    router,
    data: {
        messages: []
    },

    created() {
        this.fetchMessages();
        window.Echo.private('chat')
            .listen('App\\Events\\MessageSent', (e) => {
                console.log(e);
                this.sentMessages.push(e);
            });
    },
    mounted() {

    },

    methods: {
        fetchMessages() {
            axios.get('/chat/messages').then(response => {
                this.messages = response.data;
            });
        },

        addMessage(message) {
            this.messages.push(message);
            axios.post('/chat/messages', message).then(response => {
                console.log(response.data);
            });
        }
    }


});
