@extends('templates.public.master')

@section('css')
    <style>

    </style>
@stop
@section('main')
    <div class="container">
        <div class="row">

            <div class="col-md-7 col-sm-8">
                <!-- block content -->
                <div class="block-content">
                    <!-- single-post box -->
                    <div class="single-post-box">
                        <div class="title-post">
                            <h1>Change Password</h1>
                        </div>
                        <div class="login-box-body">
                            <p class="login-box-msg">{{ session('msgrs')?session('msgrs'): ''  }}</p>
                            <form action="{{ route('auth.reset.updatepassword') }}" method="post">
                                {!! csrf_field() !!}

                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="">Email: </div>
                                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <input type="email" name="email" class="form-control"
                                           value="{{ isset($email) ? $email : old('email') }}"
                                           placeholder="Email">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <lable class="">Password: </lable>
                                    <input type="password" name="password" class="form-control"
                                           placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                    @endif
                                </div>
                                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                    <lable class="">Retype password: </lable>
                                    <input type="password" name="password_confirmation" class="form-control"
                                           placeholder="Retype password">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Change Password</button>
                            </form>


                        </div>
                    </div>
                    <!-- End single-post box -->

                </div>
                <!-- End block content -->
            </div>
            <div class="col-md-2 col-sm-0">
                <!-- sidebar -->
                @widget('SmallSidebar')
            </div>
            <div class="col-md-3 col-sm-4">
                <!-- sidebar -->
                @widget('Sidebar')
                <!-- End sidebar -->
            </div>

        </div>

    </div>
@stop
@section('js')

@endsection
