@extends('templates.public.master')

@section('css')

    <link href="/templates/core/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/templates/core/bootstrap-star-rating/themes/krajee-svg/theme.css" media="all" rel="stylesheet" type="text/css" />
{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/2.0.0/css/star-rating.min.css" />--}}
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
@stop
@section('main')
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-0">
                <!-- sidebar -->
                @widget('SmallSidebar')
            </div>
            <div class="col-md-7 col-sm-8">
                <!-- block content -->
                <div class="block-content">
                    <!-- single-post box -->
                    <div class="single-post-box">

                        <div class="title-post">
                            <h1>{{ $post->title }}</h1>
                            <ul class="post-tags">
                                <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
                                <li><i class="fa fa-user"></i>by <a href="#">{{ $post->user->name }}</a></li>
{{--                                <li><a href="#"><i class="fa fa-comments-o"></i><span>0</span></a></li>--}}
                                <li><i class="fa fa-eye"></i>{{  views($post)->count() }}</li>
                            </ul>
                        </div>
                        @php
                            // create instance
                            $hinhanh = $post->picture;
                            $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
                            if( !empty( $hinhanh ) && file_exists( $path ) ) {
                              $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 653, 322) ;
                            } else {
                              $anh = '';
                            }
                        @endphp
                        <div class="post-gallery">
                            <img src="{{ $anh }}" alt="">
                            <span class="image-caption">{{ $post->title }}</span>
                        </div>

                        <div class="post-content">
                            {!! $post->content  !!}
                        </div>

                        <div class="post-tags-box">
                            <ul class="tags-box">
                                <li><i class="fa fa-tags"></i><span>Tags:</span></li>
                                @foreach($post->tags as $tag)
                                    <li><a href="#">{{ ucwords($tag->tag) }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="post-tags-box">
                            @if(\Auth::check())
                                @php
                                    $action =  route('public.review');
                                @endphp
                            @endif
                            <form action="{{!empty($action) ? $action : '' }}" method="POST">
                                {{ csrf_field() }}
                                <div class="card">
                                    <div class="container-fliud">
                                        <div class="wrapper row">
                                            <h4 class="product-title">Đánh giá bài viết <span class="label label-success">{{ $post->ratings->count() }} lượt đánh giá</span></h4>
                                            <div class="rating">
                                                <input id="input-1" required name="rate" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="{{ $post->averageRating() }}" data-size="lg">
                                                <input type="hidden" name="id" required="" value="{{ $post->id }}">

                                                <br/>
                                                @if(\Auth::check() && !session('rating-'.$post->id.'-'.\Auth::id()) )
                                                <button class="btn btn-danger ">Gửi đánh giá</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="share-post-box">
                            <ul class="share-box">
                                <li><i class="fa fa-share"></i><span>Share Post</span></li>
                                <li>
                                    <a class="facebook" onclick="
	                        window.open(
	                          'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),
	                          'facebook-share-dialog',
	                          'width=626,height=436');
	                        return false;" target="_blank" href="#" ><i class="fa fa-facebook"></i>Share on Facebook</a>
                                </li>
                                <li><a class="twitter" target="_blank" href="http://twitter.com/share?text={{ $post->title }}&url={{ Request::fullUrl() }}"><i class="fa fa-twitter"></i>Share on Twitter</a></li>

                            </ul>

                        </div>

                        <div class="prev-next-posts">
                            @foreach($post->categories as $cat)
                                @php
                                    if($catSlug == str_slug($cat->name)){
                                        $category = $cat;
                                    }
                                @endphp
                            @endforeach
                            @foreach($category->posts()->where('post_id','!=',$post->id)->inRandomOrder()->limit(2)->get() as $item)
                                @php
                                    $arHref = [
                                      str_slug($category->name),
                                      str_slug($item->title),
                                      $item->id
                                    ];
                                    // if(empty($cat)) {
                                    //   dd($post);
                                    // }
                                    $hrefPost = route('public.detail',$arHref);
                                    $arHref1 = [
                                      str_slug($category->name),
                                      $category->id
                                    ];
                                    $hrefCat = route('public.category',$arHref1);
                                @endphp
                                <div class="prev-post">
                                    <div class="post-content">
                                        <h2><a href="{{$hrefPost }}" title="prev post">{{ $item->title }}</a></h2>
                                        <ul class="post-tags">
                                            <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($item->created_at)) }}</li>
{{--                                            <li><a href="#"><i class="fa fa-comments-o"></i><span>11</span></a></li>--}}
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="carousel-box owl-wrapper">
                            <div class="title-section">
                                <h1><span>You may also like</span></h1>
                            </div>
                            @widget('PopularIndex')

                        </div>
                        <!-- End carousel box -->

                        <!-- contact form box -->
{{--                        <div class="contact-form-box">--}}
{{--                            <div class="title-section">--}}
{{--                                <h1><span>Leave a Comment</span> <span class="email-not-published">Your email address will not be published.</span>--}}
{{--                                </h1>--}}
{{--                            </div>--}}
{{--                            <form id="comment-form">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-4">--}}
{{--                                        <label for="name">Name*</label>--}}
{{--                                        <input id="name" name="name" type="text">--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-4">--}}
{{--                                        <label for="mail">E-mail*</label>--}}
{{--                                        <input id="mail" name="mail" type="text">--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-4">--}}
{{--                                        <label for="website">Website</label>--}}
{{--                                        <input id="website" name="website" type="text">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <label for="comment">Comment*</label>--}}
{{--                                <textarea id="comment" name="comment"></textarea>--}}
{{--                                <button type="submit" id="submit-contact">--}}
{{--                                    <i class="fa fa-comment"></i> Post Comment--}}
{{--                                </button>--}}
{{--                            </form>--}}
{{--                        </div>--}}
                        <!-- End contact form box -->

                    </div>
                    <!-- End single-post box -->

                </div>
                <!-- End block content -->
            </div>

            <div class="col-md-3 col-sm-4">
                <!-- sidebar -->
                @widget('Sidebar')
                <!-- End sidebar -->
            </div>

        </div>

    </div>
@stop
@section('js')

    <script src="/templates/core/bootstrap-star-rating/js/star-rating.min.js"></script>
    <script src="/templates/core/bootstrap-star-rating/themes/krajee-svg/theme.js"></script>
    <script type="text/javascript">

        $("#input-id").rating();

    </script>
@endsection
