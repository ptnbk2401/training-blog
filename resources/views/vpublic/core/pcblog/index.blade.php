@extends('templates.public.master')

@section('css')
    <style>

    </style>
@stop
@section('main')
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-0">
                <!-- sidebar -->
                @widget('SmallSidebar')
            </div>
            <div class="col-md-7 col-sm-8">
                <!-- block content -->
                <div class="block-content">
                    <!-- grid box -->
                    <div class="grid-box">
                        <div class="title-section">
                            <h1><span class="world">{{$cat->name}}</span></h1>
                        </div>
                        @foreach ($objPosts as $key=>$post)
                            @php
                                $arHref = [
                                  str_slug($cat->name),
                                  str_slug($post->title),
                                  $post->id
                                ];
                                // if(empty($cat)) {
                                //   dd($post);
                                // }
                                $hrefPost = route('public.detail',$arHref);
                                $arHref1 = [
                                  str_slug($cat->name),
                                  $cat->id
                                ];
                                $hrefCat = route('public.category',$arHref1);
                                $open = ($key%2==0) ?  1 : 0;
                                $close = 0;
                            @endphp
                            @php
                                // create instance
                                $hinhanh = $post->picture;
                                $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
                                if( !empty( $hinhanh ) && file_exists( $path ) ) {
                                  $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 311, 233) ;
                                } else {
                                  $anh = '';
                                }
                            @endphp
                            @if($open)
                                @php
                                    $close = 0;
                                @endphp
                                <div class="row">
                            @else
                                @php
                                    $close = 1;
                                @endphp
                            @endif

                                <div class="col-md-6">
                                    <div class="news-post image-post2">
                                        <div class="post-gallery">
                                            <img src="{{ $anh }}" alt="">
                                            <div class="hover-box">
                                                <div class="inner-hover">
                                                    <a class="category-post world" href="{{$hrefCat}}">{{$cat->name}}</a>
                                                    <h2><a href="{{ $hrefPost }}">{{ str_limit($post->title,50) }}</a></h2>
                                                    <ul class="post-tags">
                                                        <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
                                                        <li><i class="fa fa-eye"></i>{{  views($post)->count() }}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post-content">
                                            <p>{{ $post->preview_text }}</p>
                                        </div>
                                    </div>
                                </div>
                            @if($close || ( !$close && $key==count($objPosts)-1 ))
                            </div>
                            @endif
                        @endforeach

                    </div>
                    <!-- End grid box -->

                    <!-- pagination box -->
                    <div class="pagination-box">
                        <ul class="pagination-list">
                            {{$objPosts->links()}}
                        </ul>
                    </div>
                    <!-- End Pagination box -->

                </div>
                <!-- End block content -->
            </div>

            <div class="col-md-3 col-sm-4">
                <!-- sidebar -->
                @widget('Sidebar')
                <!-- End sidebar -->
            </div>

        </div>

    </div>
@stop
@section('js')

@endsection
