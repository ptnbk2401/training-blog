@extends('templates.public.master')

@section('css')
<style>

</style>
@stop
@section('main')
<div class="container">
  <div class="row">
    <div class="col-md-7 col-sm-8">
      <!-- block content -->
      <div class="block-content">

        <!-- grid box -->
        <div class="grid-box">
          <div class="image-slider">
            <ul class="bxslider">
              @widget('TopSliderIndex')
            </ul>
          </div>
        </div>
        <!-- End grid box -->

        <!-- grid box -->

          @widget('TodayFeatured')

        <!-- End grid box -->

        <!-- carousel box -->
        <div class="carousel-box owl-wrapper">

          <div class="title-section">
            <h1><span>Popular</span></h1>
          </div>
          @widget('PopularIndex')

        </div>
        <!-- End carousel box -->


{{--        <!-- google addsense -->--}}
{{--        <div class="advertisement">--}}
{{--          <div class="desktop-advert">--}}
{{--            <span>Advertisement</span>--}}
{{--            <img src="/templates/core/upload/addsense/600x80.jpg" alt="">--}}
{{--          </div>--}}
{{--          <div class="tablet-advert">--}}
{{--            <span>Advertisement</span>--}}
{{--            <img src="/templates/core/upload/addsense/468x60-white.jpg" alt="">--}}
{{--          </div>--}}
{{--          <div class="mobile-advert">--}}
{{--            <span>Advertisement</span>--}}
{{--            <img src="/templates/core/upload/addsense/300x250.jpg" alt="">--}}
{{--          </div>--}}
{{--        </div>--}}
{{--        <!-- End google addsense -->--}}

        <!-- article box -->
        @widget('LatestArticlesIndex')

      </div>
      <!-- End block content -->
    </div>
    <div class="col-md-2 col-sm-0">
      <!-- sidebar -->
      @widget('SmallSidebar')
    </div>
    <div class="col-md-3 col-sm-4">
      <!-- sidebar -->
      @widget('Sidebar')
      <!-- End sidebar -->
    </div>

  </div>

</div>
@stop
@section('js')

@endsection
