<!doctype html>
<html lang="en" class="no-js">

<!-- Mirrored from nunforest.com/hotmagazine/default/home3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Apr 2019 01:12:01 GMT -->
<head>
  <title>HotMagazine</title>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic' rel='stylesheet' type='text/css'>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/fontawesome.min.css" rel="stylesheet">
  
{{--  <link rel="stylesheet" type="text/css"  href="/templates/core/css/bootstrap.min.css" media="screen">--}}
  <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css"  href="/templates/core/css/jquery.bxslider.css" media="screen">
  <link rel="stylesheet" type="text/css"  href="/templates/core/css/font-awesome.css" media="screen">
  <link rel="stylesheet" type="text/css"  href="/templates/core/css/magnific-popup.css" media="screen">  
  <link rel="stylesheet" type="text/css"  href="/templates/core/css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css"  href="/templates/core/css/owl.theme.css" media="screen">
  <link rel="stylesheet" type="text/css"  href="/templates/core/css/ticker-style.css"/>
  <link rel="stylesheet" type="text/css"  href="/templates/core/css/style.css" media="screen">
  <link rel="stylesheet" type="text/css"  href="/templates/core/css/mystyle.css" media="screen">
  <link rel="stylesheet" type="text/css" href="https://skywalkapps.github.io/bootstrap-notifications/stylesheets/bootstrap-notifications.css">

  <style>
    .pagination-box ul.pagination-list li.active span {
      border: 1px solid #f44336;
      background: #f44336;
      color: #ffffff;
      border-radius: 3px;
      font-size: 12px;

    }
    #login-modal, #alert-modal {
      z-index: 99999;
    }
    #alert-modal .modal-dialog {
      top: 230px;
    }
    /* CSS used here will be applied after bootstrap.css */

    .dropdown {
      display:inline-block;
      margin-left:20px;
      padding:10px;
    }


    .glyphicon-bell {

      font-size:1.5rem;
    }

    .notifications {
      min-width:420px;
    }

    .notifications-wrapper {
      overflow:auto;
      max-height:250px;
    }

    .menu-title {
      color:#ff7788;
      font-size:1.5rem;
      display:inline-block;
    }

    .glyphicon-circle-arrow-right {
      margin-left:10px;
    }


    .notification-heading, .notification-footer  {
      padding:2px 10px;
    }


    .dropdown-menu.divider {
      margin:5px 0;
    }

    .item-title {

      font-size:1.3rem;
      color:#000;

    }

    .notifications a.content {
      text-decoration:none;
      background:#ccc;

    }

    .notification-item {
      padding:10px;
      margin:5px;
      background:#ccc;
      border-radius:4px;
    }





  </style>

  @yield('css')
</head>
<body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=844118862589463&autoLogAppEvents=1"></script>

  <!-- Container -->
  <div id="container">

    <!-- Header
        ================================================== -->
    <header class="clearfix third-style">
      <!-- Bootstrap navbar -->
      @widget('Header')
      <!-- End Bootstrap navbar -->
    </header>
    <!-- End Header -->

    <!-- block-wrapper-section
      ================================================== -->
    <section class="block-wrapper">
      @yield('main')
    </section>
    <!-- End block-wrapper-section -->

    <!-- footer 
      ================================================== -->
    <footer>
      @widget('Footer')
    </footer>
    <!-- End footer -->

  </div>
  <!-- End Container -->
    @widget('ModelPopup')

  <!-- END # MODAL LOGIN -->
  <script type="text/javascript" src="/templates/core/js/jquery.min.js"></script>
  <script type="text/javascript" src="/templates/core/js/jquery.migrate.js"></script>
  <script type="text/javascript" src="/templates/core/js/jquery.bxslider.min.js"></script>
  <script type="text/javascript" src="/templates/core/js/jquery.magnific-popup.min.js"></script>
  <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="/templates/core/js/jquery.ticker.js"></script>
  <script type="text/javascript" src="/templates/core/js/jquery.imagesloaded.min.js"></script>
    <script type="text/javascript" src="/templates/core/js/jquery.isotope.min.js"></script>
  <script type="text/javascript" src="/templates/core/js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="/templates/core/js/retina-1.1.0.min.js"></script>
  <script type="text/javascript" src="/templates/core/js/plugins-scroll.js"></script>
  <script type="text/javascript" src="/templates/core/js/script.js"></script>
  <script type="text/javascript" src="/templates/core/js/myscript.js"></script>
  @yield('js')
  <script>
    $(function () {
      @if ($errors->any())
      $('#login-modal').modal('show');
      @endif
      @if (session('msg-al'))
      $('#alert-modal').modal('show');
      @endif
    })
  </script>

</body>

<!-- Mirrored from nunforest.com/hotmagazine/default/home3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Apr 2019 01:12:30 GMT -->
</html>