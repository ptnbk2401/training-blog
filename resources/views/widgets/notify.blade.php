<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    <i class="fa fa-bell-o"></i>
    <span data-count="{{ !empty($notifies) ? count($notifies) : 0 }}" class="label label-warning">{{ !empty($notifies) ? count($notifies) : 0 }}</span>
</a>
<ul class="dropdown-menu">
    <li class="header">You have <span>{{ !empty($notifies) ? count($notifies) : 0 }}</span> notifications</li>
    <li>
        <!-- inner menu: contains the actual data -->
        <ul class="menu">
            @if(!empty($notifies))
                @php
                    \Carbon\Carbon::setLocale('vi');
                    $now     = \Carbon\Carbon::now();
                @endphp
            @foreach( $notifies as $notify)
            <li><!-- start message -->
                <a href="#">
                    <div class="pull-left">
                        @php
                            $from_user = \App\User::find($notify->from_user_id);
                            $avatar = !empty($from_user->avatar)? $from_user->avatar->avatar : '/templates/core/images/avatar-profile.png' ;
                        $dt = \Carbon\Carbon::createFromTimeString($notify->created_at);
                        @endphp
                        <img src="{{ $avatar }}" style='width: 35px; height: 35px' class="img-circle" alt="User Image">
                    </div>
                    <h4>
                        {{$from_user->name}}
                        @if ( $now->diffInHours($dt) <= 23 )
                        <small><i class="fa fa-clock-o"></i> {{$dt->diffForHumans($now) }}</small>
                        @else
                        <small><i class="fa fa-clock-o"></i> {{ date('d/m/Y',strtotime($dt)) }}</small>
                        @endif

                    </h4>
                    <p>{{ $notify->content }}</p>
                </a>
            </li>
            @endforeach
            @endif
        </ul>
    </li>
    <li class="footer"><a href="{{ route('profile') }}">View all</a></li>
</ul>

