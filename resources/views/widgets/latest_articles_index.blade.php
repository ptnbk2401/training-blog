<div class="article-box">
    <div class="title-section">
        <h1 id="lastest_article"><span>Latest Articles</span></h1>
    </div>
    @foreach ($objPost as $post)
        @php
            $cat = $post->categories()->first();
            $arHref = [
              str_slug($cat->name),
              str_slug($post->title),
              $post->id
            ];
            // if(empty($cat)) {
            //   dd($post);
            // }
            $hrefPost = route('public.detail',$arHref);
            $arHref1 = [
              str_slug($cat->name),
              $cat->id
            ];
            $hrefCat = route('public.category',$arHref1);
        @endphp
        @php
            // create instance
            $hinhanh = $post->picture;
            $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
            if( !empty( $hinhanh ) && file_exists( $path ) ) {
              $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 198, 146) ;
            } else {
              $anh = '';
            }
        @endphp
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-4">
                    <div class="post-gallery">
                        <img alt="" src="{{ $anh }}">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="post-content">
                        <h2><a href="{{ $hrefPost }}">{{ str_limit($post->title,50) }}</a></h2>
                        <ul class="post-tags">
                            <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
                            <li><i class="fa fa-user"></i>by <a href="#">{{ $post->user->name }}</a></li>
{{--                            <li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>--}}
                            <li><i class="fa fa-eye"></i>{{  views($post)->count() }}</li>
                        </ul>
{{--                        <span class="post-rating">--}}
{{--                    <i class="fa fa-star"></i>--}}
{{--                    <i class="fa fa-star"></i>--}}
{{--                    <i class="fa fa-star"></i>--}}
{{--                    <i class="fa fa-star"></i>--}}
{{--                    <i class="fa fa-star"></i>--}}
{{--                  </span>--}}
                        <p>{{ str_limit($post->preview_text,150) }}</p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach


</div>
<!-- End article box -->

<!-- Pagination box -->
<div class="pagination-box">
    <ul class="pagination-list">
        {{$objPost->links()}}
    </ul>
{{--    <p>Page 1 of 9</p>--}}
</div>
<!-- End Pagination box -->