<!-- BEGIN # MODAL LOGIN -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <img class="img-circle" id="img_logo" src="/templates/core/images/smile.png">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
            </div>
            <!-- Begin # DIV Form -->
            <div id="div-forms">

                <!-- Begin # Login Form -->
                <form id="login-form" action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <div id="div-login-msg">
                            <p class="login-box-msg">{{ trans('adminlte::adminlte.login_message') }}</p>
                            <div id="icon-login-msg" class="fa fa-chevron-right">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span><br>
                                @endif
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <span id="text-login-msg">Type your email and password.</span>
                        </div>
                        <input id="login_username" class="form-control" type="email" name="email"
                               placeholder="Email (type ERROR for error effect)" required>
                        <input id="login_password" class="form-control" type="password" name="password"
                               placeholder="Password" required>
                        <div class="checkbox">
                            <label> <input type="checkbox"> Remember me </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                        </div>
                        <div>
                            <button id="login_lost_btn" type="button" class="btn btn-link">Lost Password?</button>
                            <button id="login_register_btn" type="button" class="btn btn-link">Register</button>
                        </div>
                    </div>
                </form>
                <!-- End # Login Form -->

                <!-- Begin | Lost Password Form -->
                <form id="lost-form" style="display:none;" action="{{ route('auth.reset.password') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div id="div-lost-msg">
                            <div id="icon-lost-msg" class="fa fa-chevron-right"></div>
                            <span id="text-lost-msg">Type your e-mail.</span>
                        </div>
                        <input id="lost_email" class="form-control" type="text"
                               placeholder="E-Mail (type ERROR for error effect)" name="email" required>
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Send</button>
                        </div>
                        <div>
                            <button id="lost_login_btn" type="button" class="btn btn-link">Log In</button>
                            <button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
                        </div>
                    </div>
                </form>
                <!-- End | Lost Password Form -->

                <!-- Begin | Register Form -->
                <form id="register-form" style="display:none;" action="{{ url(config('adminlte.register_url', 'register')) }}"  method="post">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <div id="div-register-msg">
                            <div id="icon-register-msg" class="fa fa-chevron-right"></div>
                            <span id="text-register-msg">Register an account.</span>
                        </div>
                        <input id="register_username"  name="name"  class="form-control" type="text"
                               placeholder="Name (type ERROR for error effect)" required>
                        <input id="register_email" name="email"  class="form-control" type="text" placeholder="E-Mail" required>
                        <input id="register_password" name="password"  class="form-control" type="password" placeholder="Password"
                               required>
                        <input type="password" name="password_confirmation" class="form-control"
                               >
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                        </div>
                        <div>
                            <button id="register_login_btn" type="button" class="btn btn-link">Log In</button>
                            <button id="register_lost_btn" type="button" class="btn btn-link">Lost Password?</button>
                        </div>
                    </div>
                </form>
                <!-- End | Register Form -->

            </div>
            <!-- End # DIV Form -->

        </div>
    </div>
</div>


<!-- BEGIN # MODAL LOGIN -->
<div class="modal fade" id="alert-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <img class="img-circle" id="img_logo" src="/templates/core/images/smile.png">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
            </div>
            <div id="div-forms">
                <div class="row">
                    @if(session('msg-al'))
                        <h4 class="text-center" style="color: violet; margin-bottom: 50px;">{{session('msg-al')}}</h4>
                    @endif
                </div>

            </div>

        </div>
    </div>
</div>