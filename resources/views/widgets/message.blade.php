
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-envelope-o"></i>
        <span data-count="{{ !empty($messages) ? count($messages) : 0 }}" class="label label-success">{{ !empty($messages) ? count($messages) : 0 }}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <span>{{ !empty($messages) ? count($messages) : 0 }}</span>  messages</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                @if(!empty($messages))
                    @php
                        \Carbon\Carbon::setLocale('vi');
                        $now     = \Carbon\Carbon::now();
                    @endphp
                    @foreach( $messages as $message)
                        <li><!-- start message -->
                            <a href="#">
                                <div class="pull-left">
                                    @php
                                        $from_user = \App\User::find($message->user_id);
                                        if(!empty($from_user->avatar)) {
                                            $hinhanh = $from_user->avatar->avatar;
                                            $path = storage_path('app/public/media/files/users/' .$from_user->avatar->avatar) ;
                                            if( !empty( $hinhanh ) && file_exists( $path ) ) {
                                              $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'users', 40, 40) ;
                                            } else {
                                              $anh = '/templates/core/images/avatar-profile.png';
                                            }
                                        }
                                        else {
                                            $anh = '/templates/core/images/avatar-profile.png';
                                        }
                                    $dt = \Carbon\Carbon::createFromTimeString($message->created_at);
                                    @endphp
                                    <img src="{{ $anh }}" style='width: 35px; height: 35px' class="img-circle" alt="User Image">
                                </div>
                                <h4>
                                    {{$from_user->name}}
                                    @if ( $now->diffInHours($dt) <= 23 )
                                        <small><i class="fa fa-clock-o"></i> {{$dt->diffForHumans($now) }}</small>
                                    @else
                                        <small><i class="fa fa-clock-o"></i> {{ date('d/m/Y',strtotime($dt)) }}</small>
                                    @endif

                                </h4>
                                <p>{{ $message->message }}</p>
                            </a>
                        </li>
                @endforeach
            @endif
                <!-- end message -->

            </ul>
        </li>
        <li class="footer"><a href="{{ route('message') }}">See All Messages</a></li>
    </ul>

