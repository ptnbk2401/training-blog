<div class="owl-carousel" data-num="3">
  @foreach ($objItems as $item)
  @php
    $arCat = [];
    foreach ($item->categories as $cat){
        $arCat[] = $cat->name;
    }
      
  @endphp
  @php
    $arHref = [
      str_slug($cat->name),
      str_slug($item->title),
      $item->id
    ];
    $hrefPost = route('public.detail',$arHref);
    $arHref1 = [
      str_slug($cat->name),
      $cat->id
    ];
    $hrefCat = route('public.category',$arHref1);
  @endphp
  @php
    // create instance
    $hinhanh = $item->picture;
    $path = storage_path('app/public/media/files/posts/' .$item->picture) ;
    if( !empty( $hinhanh ) && file_exists( $path ) ) {
      $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 100, 88) ;
    } else {
      $anh = '';
    }
  @endphp
  <div class="item list-post">
    <img src="{{  $anh }}" alt="">
    <div class="post-content">
      <a href="{{ $hrefCat }}">{{ $cat->name }}</a>
      <h2><a href="{{ $hrefPost }}">{{ $item->title }}</a></h2>
      <ul class="post-tags">
        <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($item->created_at)) }}</li>
      </ul>
    </div>
  </div>
  @endforeach
</div>