<div class="sidebar large-sidebar">

        <div class="widget search-widget">
          <form role="search" class="search-form" method="get" action="{{ route('public.search') }}">
            <input type="text" id="search" name="c" placeholder="Search here" value="{{ !empty(request('search')) ? request('c') : '' }}">
            <button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
          </form>
        </div>

{{--        <div class="widget social-widget">--}}
{{--          <div class="title-section">--}}
{{--            <h1><span>Stay Connected</span></h1>--}}
{{--          </div>--}}
{{--          <ul class="social-share">--}}
{{--            <li>--}}
{{--              <a href="#" class="rss"><i class="fa fa-rss"></i></a>--}}
{{--              <span class="number">9,455</span>--}}
{{--              <span>Subscribers</span>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>--}}
{{--              <span class="number">56,743</span>--}}
{{--              <span>Fans</span>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>--}}
{{--              <span class="number">43,501</span>--}}
{{--              <span>Followers</span>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--              <a href="#" class="google"><i class="fa fa-google-plus"></i></a>--}}
{{--              <span class="number">35,003</span>--}}
{{--              <span>Followers</span>--}}
{{--            </li>--}}
{{--          </ul>--}}
{{--        </div>--}}

        <div class="widget features-slide-widget">
          <div class="title-section">
            <h1><span>Featured Posts</span></h1>
          </div>
          <div class="image-post-slider">
            <ul class="bxslider">
              @foreach ($objPost as $key=>$post)
              @php
                $cat = $post->categories()->first();
                $arHref = [
                  str_slug($cat->name),
                  str_slug($post->title),
                  $post->id
                ];
                // if(empty($cat)) {
                //   dd($post);
                // }
                $hrefPost = route('public.detail',$arHref);
              @endphp
                @php
                  // create instance
                  $hinhanh = $post->picture;
                  $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
                  if( !empty( $hinhanh ) && file_exists( $path ) ) {
                    $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 263, 214) ;
                  } else {
                    $anh = '';
                  }
                @endphp
              <li>
                <div class="news-post image-post2">
                  <div class="post-gallery">
                    <img src="{{ $anh }}"  >
                    <div class="hover-box">
                      <div class="inner-hover">
                        <h2><a href="{{  $hrefPost }}">{{ str_limit($post->title,30) }}</a></h2>
                        <ul class="post-tags">
                          <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
                          <li><i class="fa fa-eye"></i>{{  views($post)->count() }}</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              @php
                if($key==2) break;
              @endphp
              @endforeach
            </ul>
          </div>
          <ul class="list-posts">
            @foreach ($objPost as $key=>$post)
              @if($key>2)
              @php
                $cat = $post->categories()->first();
                $arHref = [
                  str_slug($cat->name),
                  str_slug($post->title),
                  $post->id
                ];
                // if(empty($cat)) {
                //   dd($post);
                // }
                $hrefPost = route('public.detail',$arHref);
              @endphp
                @php
                  // create instance
                  $hinhanh = $post->picture;
                  $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
                  if( !empty( $hinhanh ) && file_exists( $path ) ) {
                    $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 70, 61) ;
                  } else {
                    $anh = '';
                  }
                @endphp
                <li>
                  <img src="{{ $anh  }}"  alt="">
                  <div class="post-content">
                    <h2><a href="{{  $hrefPost }}">{{ str_limit($post->title,40) }}</a></h2>
                    <ul class="post-tags">
                      <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
                    </ul>
                  </div>
                </li>
              @endif
              @endforeach
          </ul>
        </div>

{{--        <div class="advertisement">--}}
{{--          <div class="desktop-advert">--}}
{{--            <span>Advertisement</span>--}}
{{--            <img src="/templates/core/upload/addsense/250x250.jpg" alt="">--}}
{{--          </div>--}}
{{--          <div class="tablet-advert">--}}
{{--            <span>Advertisement</span>--}}
{{--            <img src="/templates/core/upload/addsense/200x200.jpg" alt="">--}}
{{--          </div>--}}
{{--          <div class="mobile-advert">--}}
{{--            <span>Advertisement</span>--}}
{{--            <img src="/templates/core/upload/addsense/300x250.jpg" alt="">--}}
{{--          </div>--}}
{{--        </div>--}}

        <div class="widget tab-posts-widget">

          <ul class="nav nav-tabs" id="myTab">
            <li class="active">
              <a href="#option1" data-toggle="tab">Popular</a>
            </li>
            <li>
              <a href="#option2" data-toggle="tab">Recent</a>
            </li>
          </ul>

          <div class="tab-content">
            <div class="tab-pane active" id="option1">
              <ul class="list-posts">
              @foreach ($objPostPopular as $key=>$post)
                @php
                  $cat = $post->categories()->first();
                  $arHref = [
                    str_slug($cat->name),
                    str_slug($post->title),
                    $post->id
                  ];
                  // if(empty($cat)) {
                  //   dd($post);
                  // }
                  $hrefPost = route('public.detail',$arHref);
                @endphp
                @php
                  // create instance
                  $hinhanh = $post->picture;
                  $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
                  if( !empty( $hinhanh ) && file_exists( $path ) ) {
                    $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 75, 66) ;
                  } else {
                    $anh = '';
                  }
                @endphp
                  <li>
                    <img src="{{ $anh }}"   alt="">
                    <div class="post-content">
                      <h2><a href="{{  $hrefPost }}">{{ str_limit($post->title,45) }}</a></h2>
                      <ul class="post-tags">
                        <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
                      </ul>
                    </div>
                  </li>
                @endforeach
              </ul>
            </div>
            <div class="tab-pane" id="option2">
              <ul class="list-posts">
                @foreach ($objPostRecent as $key=>$post)
                @php
                  $cat = $post->categories()->first();
                  $arHref = [
                    str_slug($cat->name),
                    str_slug($post->title),
                    $post->id
                  ];
                  // if(empty($cat)) {
                  //   dd($post);
                  // }
                  $hrefPost = route('public.detail',$arHref);
                @endphp
                @php
                  // create instance
                  $hinhanh = $post->picture;
                  $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
                  if( !empty( $hinhanh ) && file_exists( $path ) ) {
                    $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 75, 66) ;
                  } else {
                    $anh = '';
                  }
                @endphp
                  <li>
                    <img src="{{ $anh }}" style="width: 75px; height: 66px" alt="">
                    <div class="post-content">
                      <h2><a href="{{  $hrefPost }}">{{ str_limit($post->title,45) }}</a></h2>
                      <ul class="post-tags">
                        <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
                      </ul>
                    </div>
                  </li>
                @endforeach
              </ul>                   
            </div>
          </div>
        </div>

        {{-- <div class="widget post-widget">
          <div class="title-section">
            <h1><span>Featured Video</span></h1>
          </div>
          <div class="news-post video-post">
            <img alt="" src="/templates/core/upload/news-posts/video-sidebar.jpg">
            <a href="https://www.youtube.com/watch?v=LL59es7iy8Q" class="video-link"><i class="fa fa-play-circle-o"></i></a>
            <div class="hover-box">
              <h2><a href="single-post.html">Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. </a></h2>
              <ul class="post-tags">
                <li><i class="fa fa-clock-o"></i>27 may 2013</li>
              </ul>
            </div>
            <p></p>
          </div>
          <p>Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede. Donec nec justo eget felis facilisis. </p>
        </div> --}}
      </div>