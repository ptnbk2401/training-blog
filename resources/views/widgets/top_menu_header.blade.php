<div class="container">

  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="{{route('public.index')}}"><img src="/templates/core/images/small-logo.png" alt=""></a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav navbar-right">
      <li class="drop"><a class="home" href="{{ route('public.index') }}">Home</a>
        <ul class="dropdown">
        </ul>
      </li>
      @php
        $arClass = ['world','travel','tech','fashion','video','sport','food'];
      @endphp
      @foreach ($objCat as $key=>$cat)
          @php
            $arHref1 = [
              str_slug($cat->name),
              $cat->id
            ];
            $hrefCat = route('public.category',$arHref1);
          @endphp

         <li><a class="{{ $arClass[$key] }}" href="{{ $hrefCat }}">{{ $cat->name }}</a>
            <div class="megadropdown">
              <div class="container">
                <div class="inner-megadropdown travel-dropdown">

                  <div class="owl-wrapper">
                    <h1>Bài viết mới</h1>
                    <div class="owl-carousel" data-num="4">
                      @foreach ($cat->posts()->orderBy('id','DESC')->limit(5)->get() as $post)
                        @php
                          $arHref = [
                            str_slug($cat->name),
                            str_slug($post->title),
                            $post->id
                          ];
                          $hrefPost = route('public.detail',$arHref);
                        @endphp
                        @php
                            // create instance
                            $hinhanh = $post->picture;
                            $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
                            if( !empty( $hinhanh ) && file_exists( $path ) ) {
                              $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 260, 193) ;
                            } else {
                              $anh = '';
                            }
                        @endphp
                         <div class="item news-post standard-post">
                            <div class="post-gallery">
                              <a href="{{ $hrefPost }}"><img src="{{ $anh }}" alt="" ></a>
                            </div>
                            <div class="post-content">
                              <h2><a href="{{ $hrefPost }}">{{ $post->title }}</a></h2>
                              <ul class="post-tags">
                                <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
                                <li><i class="fa fa-eye"></i>{{  views($post)->count() }}</li>
                              </ul>
                            </div>
                          </div>
                      @endforeach
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </li>
          @if ($key==6)
            @php
              break;
            @endphp
          @endif
      @endforeach

      <li class="drop"><a class="features" href="#">Khác</a>
        <ul class="dropdown features-dropdown">
          @foreach ($objCat as $key=>$cat)
              @if ($key>6)
                @php
                  $arHref1 = [
                    str_slug($cat->name),
                    $cat->id
                  ];
                  $hrefCat = route('public.category',$arHref1);
                @endphp
                <li><a href="{{ $hrefCat }}">{{ $cat->name }}</a></li>
              @endif
          @endforeach
        </ul>
      </li>



    </ul>

  </div>
  <!-- /.navbar-collapse -->
</div>