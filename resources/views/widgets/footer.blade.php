<div class="container">
  <div class="footer-widgets-part">
    <div class="row">
      <div class="col-md-3">
        <div class="widget text-widget">
          <h1>About</h1>
          <p>NguyenIT - Blog Training</p>
        </div>
        <div class="widget social-widget">
          <h1>Stay Connected</h1>
          <ul class="social-icons">
            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" class="youtube"><i class="fa fa-youtube"></i></a></li>
            <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
            <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#" class="vimeo"><i class="fa fa-vimeo-square"></i></a></li>
            <li><a href="#" class="dribble"><i class="fa fa-dribbble"></i></a></li>
            <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
            <li><a href="#" class="flickr"><i class="fa fa-flickr"></i></a></li>
            <li><a href="#" class="rss"><i class="fa fa-rss"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-3">
        <div class="widget posts-widget">
          <h1>Bài viết ngẩu nhiên</h1>
          <ul class="list-posts">
            @foreach ($objItems as $item)
              @php
                $arCat = [];
                foreach ($item->categories as $cat){
                    $arCat[] = $cat->name;
                }
                  
              @endphp
              @php
                $arHref = [
                  str_slug($cat->name),
                  str_slug($item->title),
                  $item->id
                ];
                $hrefPost = route('public.detail',$arHref);
                $arHref1 = [
                  str_slug($cat->name),
                  $cat->id
                ];
                $hrefCat = route('public.category',$arHref1);
              @endphp
              <li>
                <img src="{{ asset('/storage/media/files/posts/' .$item->picture) }}" alt="">
                <div class="post-content">
                  <a href="{{ $hrefCat }}">{{ $cat->name }}</a>
                  <h2><a href="{{ $hrefPost }}">{{ $item->title }}</a></h2>
                  <ul class="post-tags">
                    <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($item->created_at)) }}</li>
                  </ul>
                </div>
              </li>
            @endforeach
          </ul>
        </div>
      </div>
      <div class="col-md-3">
        <div class="widget categories-widget">
          <h1>Danh mục Hot</h1>
          <ul class="category-list">
            @foreach ($objCat as $cat)
              @php
                $arHref1 = [
                  str_slug($cat->name),
                  $cat->id
                ];
                $hrefCat = route('public.category',$arHref1);
              @endphp
               <li>
                <a href="{{ $hrefCat }}">{{ $cat->name }} <span>{{ count($cat->posts) }}</span></a>
              </li>
            @endforeach
          </ul>
        </div>
      </div>
      <div class="col-md-3">
        <div class="widget flickr-widget">
          <h1>Flickr Photos</h1>
          <ul class="flickr-list">
            <li><a href="#"><img src="/templates/core/upload/flickr/1.jpg" alt=""></a></li>
            <li><a href="#"><img src="/templates/core/upload/flickr/2.jpg" alt=""></a></li>
            <li><a href="#"><img src="/templates/core/upload/flickr/3.jpg" alt=""></a></li>
            <li><a href="#"><img src="/templates/core/upload/flickr/4.jpg" alt=""></a></li>
            <li><a href="#"><img src="/templates/core/upload/flickr/5.jpg" alt=""></a></li>
            <li><a href="#"><img src="/templates/core/upload/flickr/6.jpg" alt=""></a></li>
          </ul>
{{--          <a href="#">View more photos...</a>--}}
        </div>
      </div>
    </div>
  </div>

</div>