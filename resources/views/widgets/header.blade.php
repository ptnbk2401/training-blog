<nav class="navbar navbar-default navbar-static-top" role="navigation">

  <!-- Top line -->
  <div class="top-line">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <ul class="top-line-list">

            <li><span class="time-now">{{ date('D, M d-Y') }}</span></li>

              @if(Auth::check())
              <li><span>{{Auth::user()->name}}</span>(
                @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                  <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                  </a>
                @else
                  <a href="#"
                     onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                  >
                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                  </a>
                  <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                    @if(config('adminlte.logout_method'))
                      {{ method_field(config('adminlte.logout_method')) }}
                    @endif
                    {{ csrf_field() }}
                  </form>
                @endif
              )</li>
              @else
              <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>
              @endif


          </ul>
        </div>  
        <div class="col-md-3">
          <ul class="social-icons">
{{--            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>--}}
{{--            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>--}}
{{--            <li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>--}}
{{--            <li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>--}}
{{--            <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>--}}
{{--            <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>--}}
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- End Top line -->

  <!-- list line posts -->
  <div class="list-line-posts">
    <div class="container">

      <div class="owl-wrapper">
        @widget('TopPostHeader')
      </div>

    </div>
  </div>
  <!-- End list line posts -->

  <!-- navbar list container -->
  <div class="nav-list-container">
    
    @widget('TopMenuHeader')
  </div>
  <!-- End navbar list container -->

</nav>