<div class="owl-carousel" data-num="2">
    @foreach ($objPost as $key=>$post)
        @php
            $cat = $post->categories()->first();
            $arHref = [
              str_slug($cat->name),
              str_slug($post->title),
              $post->id
            ];
            // if(empty($cat)) {
            //   dd($post);
            // }
            $hrefPost = route('public.detail',$arHref);
            $arHref1 = [
              str_slug($cat->name),
              $cat->id
            ];
            $hrefCat = route('public.category',$arHref1);
        @endphp
        @php
            // create instance
            $hinhanh = $post->picture;
            $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
            if( !empty( $hinhanh ) && file_exists( $path ) ) {
              $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 322, 242) ;
            } else {
              $anh = '';
            }
        @endphp
        @if($key==0||$key==2||$key==4||$key==6)
        <div class="item">
        @endif
            <div class="news-post image-post2">
                <div class="post-gallery">
                    <img src="{{ $anh }}" alt="">
                    <div class="hover-box">
                        <div class="inner-hover">
                            <a class="category-post world" href="{{ $hrefCat }}">{{ $cat->name }}</a>
                            <h2><a href="{{ $hrefPost }}">{{ str_limit($post->title,50) }}</a></h2>
                            <ul class="post-tags">
                                <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
                                <li><i class="fa fa-eye"></i>{{  views($post)->count() }}</li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        @if($key==1||$key==3||$key==5||$key==7)
        </div>
        @endif


    @endforeach

</div>