@if(!empty($objPost) && count($objPost))
<div class="grid-box">

    <div class="title-section">
        <h1><span>Today's Featured</span></h1>
    </div>
    <div class="image-post-slider">
        <ul class="bxslider">
            @foreach ($objPost as $key=>$post)
                @php
                    $cat = $post->categories()->first();
                    $arHref = [
                      str_slug($cat->name),
                      str_slug($post->title),
                      $post->id
                    ];
                    // if(empty($cat)) {
                    //   dd($post);
                    // }
                    $hrefPost = route('public.detail',$arHref);
                    $arHref1 = [
                      str_slug($cat->name),
                      $cat->id
                    ];
                    $hrefCat = route('public.category',$arHref1);
                @endphp
                <li>
                    <div class="news-post image-post2">
                        <div class="post-gallery">
                            <img src="{{ asset('/storage/media/files/posts/' .$post->picture) }}"
                                 style="width: 653px; height: 292px" alt="">
                            <div class="hover-box">
                                <div class="inner-hover">
                                    <a class="category-post world" href="{{ $hrefCat }}">{{ $cat->name }}</a>
                                    <h2><a href="{{ $hrefPost }}">{{ str_limit($post->title,50) }}</a></h2>
                                    <ul class="post-tags">
                                        <li>
                                            <i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}
                                        </li>
                                        <li><i class="fa fa-user"></i>by <a href="#">{{ $post->user->name }}</a></li>
                                        {{--                                    <li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>--}}
                                        <li><i class="fa fa-eye"></i>{{  views($post)->count() }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @if($key==2)
                    @php
                        break;
                    @endphp
                @endif
            @endforeach
        </ul>
    </div>

    <div class="row">
        <div class="col-md-6">
            <ul class="list-posts">
                @foreach ($objPost as $key=>$post)
                    @if($key>=3 && $key<6)
                        @php
                            $cat = $post->categories()->first();
                            $arHref = [
                              str_slug($cat->name),
                              str_slug($post->title),
                              $post->id
                            ];
                            // if(empty($cat)) {
                            //   dd($post);
                            // }
                            $hrefPost = route('public.detail',$arHref);
                            $arHref1 = [
                              str_slug($cat->name),
                              $cat->id
                            ];
                            $hrefCat = route('public.category',$arHref1);
                        @endphp
                        <li>
                            <img src="{{ asset('/storage/media/files/posts/' .$post->picture) }}"
                                 style="width: 100px; height: 80px" alt="">
                            <div class="post-content">
                                <h2><a href="{{ $hrefPost }}">{{ str_limit($post->title,50) }}</a></h2>
                                <ul class="post-tags">
                                    <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}
                                    </li>
                                </ul>
                            </div>
                        </li>
                        @if($key==5)
                            @php
                                break;
                            @endphp
                        @endif
                    @endif
                @endforeach
            </ul>
        </div>

        <div class="col-md-6">
            <ul class="list-posts">
                @foreach ($objPost as $key=>$post)
                    @if($key>=6)
                        @php
                            $cat = $post->categories()->first();
                            $arHref = [
                              str_slug($cat->name),
                              str_slug($post->title),
                              $post->id
                            ];
                            // if(empty($cat)) {
                            //   dd($post);
                            // }
                            $hrefPost = route('public.detail',$arHref);
                            $arHref1 = [
                              str_slug($cat->name),
                              $cat->id
                            ];
                            $hrefCat = route('public.category',$arHref1);
                        @endphp
                        <li>
                            <img src="{{ asset('/storage/media/files/posts/' .$post->picture) }}"
                                 style="width: 100px; height: 80px" alt="">
                            <div class="post-content">
                                <h2><a href="{{ $hrefPost }}">{{ str_limit($post->title,50) }}</a></h2>
                                <ul class="post-tags">
                                    <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endif
                @endforeach

            </ul>
        </div>

    </div>
</div>
@endif