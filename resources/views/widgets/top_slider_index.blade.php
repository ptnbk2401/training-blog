@foreach ($objPost as $post)
  @php
    $cat = $post->categories()->first();
    $arHref = [
      str_slug($cat->name),
      str_slug($post->title),
      $post->id
    ];
    // if(empty($cat)) {
    //   dd($post);
    // }
    $hrefPost = route('public.detail',$arHref);
    $arHref1 = [
      str_slug($cat->name),
      $cat->id
    ];
    $hrefCat = route('public.category',$arHref1);
  @endphp
    <li>
      <div class="news-post image-post">
        @php
          // create instance
          $hinhanh = $post->picture;
          $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
          if( !empty( $hinhanh ) && file_exists( $path ) ) {
            $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 670, 490) ;
          } else {
            $anh = '';
          }
        @endphp
        <img src="{{ $anh }}" alt="">
        <div class="hover-box">
          <span class="top-stories">TOP STORIES</span>
          <div class="inner-hover">
            <a class="category-post tech" href="{{ $hrefCat }}">{{ $cat->name }}</a>
            <h2><a href="{{ $hrefPost }}">{{ str_limit($post->title,50) }}</a></h2>
            <ul class="post-tags">
              <li><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</li>
              <li><i class="fa fa-user"></i>by <a href="#">{{ $post->user->name }}</a></li>
              {{-- <li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li> --}}
               <li><i class="fa fa-eye"></i>{{  views($post)->count() }}</li>
            </ul>
          </div>
        </div>
      </div>
    </li>
  @endforeach
