<div class="sidebar small-sidebar">

  <div class="widget review-widget">
    <h1>Top Reviews</h1>
    <ul class="review-posts-list">
      @foreach ($objPost as $post)
      @php
        $cat = $post->categories()->first();
        $arHref = [
          str_slug($cat->name),
          str_slug($post->title),
          $post->id
        ];
        // if(empty($cat)) {
        //   dd($post);
        // }
        $hrefPost = route('public.detail',$arHref);
      @endphp
        @php
            // create instance
            $hinhanh = $post->picture;
            $path = storage_path('app/public/media/files/posts/' .$post->picture) ;
            if( !empty( $hinhanh ) && file_exists( $path ) ) {
              $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'posts', 145, 68) ;
            } else {
              $anh = '';
            }
        @endphp
        <li>
          <img src="{{ $anh }}"   alt="">
          <h2><a href="{{ $hrefPost }}">{{ str_limit($post->title,45) }}</a></h2>
          <span class="date"><i class="fa fa-clock-o"></i>{{ date('d M Y',strtotime($post->created_at)) }}</span>

          <span class="post-rating">
              @for( $i = 1; $i<= round($post->averageRating()) ; $i++ )
                  <i class="fa fa-star"></i>
              @endfor
          </span>
        </li>
      @endforeach
      
    </ul>
  </div>

{{--  <div class="advertisement">--}}
{{--    <div class="desktop-advert">--}}
{{--      <span>Advertisement</span>--}}
{{--      <img src="/templates/core/upload/addsense/160x600.jpg" alt="">--}}
{{--    </div>--}}
{{--  </div>--}}

  <div class="widget categories-widget">
    <div class="title-section">
      <h1><span>Hot Categories</span></h1>
    </div>
    <ul class="category-list">
      @foreach ($objCat as $cat)
        @php
          $arHref1 = [
            str_slug($cat->name),
            $cat->id
          ];
          $hrefCat = route('public.category',$arHref1);
        @endphp
         <li>
          <a href="{{ $hrefCat }}">{{ $cat->name }} <span>{{ count($cat->posts) }}</span></a>
        </li>
      @endforeach
    </ul>
  </div>
</div>