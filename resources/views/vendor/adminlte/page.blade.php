@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    <style>
        .user-image {
            float: left;
            width: 25px;
            height: 25px;
            border-radius: 50%;
            margin-right: 10px;
            margin-top: -2px;
        }
        ul.menu li a h4 {
            position: relative;
        }
        ul.menu li a h4 small{
            position: absolute;
            right: 0;
            font-size: 65%;
        }
        ul.menu li a .pull-left {
            margin: 10px 5px;
        }

    </style>
    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                            {!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
            @else
            <!-- Logo -->
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>A</b>LT') !!}</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
            @endif
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">

                    <ul class="nav navbar-nav">
{{--                        <li class="dropdown messages-menu">--}}
{{--                            @widget('Message')--}}
{{--                        </li>--}}
                        <li class="dropdown notifications-menu">
                            @widget('Notify')
                        </li>
                        @if (Auth::check())
                            <li><a href="{{ route('profile') }}">
            <img src="{{$avatar = !empty(Auth::user()->avatar)? asset("/storage/media/files/users/".Auth::user()->avatar->avatar) : '/templates/core/images/avatar-profile.png'}}" class="user-image" alt="User Image"> {{ Auth::user()->name }}</a>
                            </li>
                        @endif
                        <li>
                            @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                            @else
                                <a href="#"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                >
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                    @if(config('adminlte.logout_method'))
                                        {{ method_field(config('adminlte.logout_method')) }}
                                    @endif
                                    {{ csrf_field() }}
                                </form>
                            @endif
                        </li>
                    </ul>
                </div>
                @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

        @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar Menu -->
                <ul class="sidebar-menu" data-widget="tree">
                    @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>
        @endif

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
            <div class="container">
            @endif

            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content_header')
            </section>

            <!-- Main content -->
            <section class="content">

                @yield('content')

            </section>
            <!-- /.content -->
            @if(config('adminlte.layout') == 'top-nav')
            </div>
            <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <script>
        $(function () {
            var notificationsWrapper   = $('.notifications-menu');
            var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
            var notificationsCountElem = notificationsToggle.find('span[data-count]');
            var notificationsCount     = parseInt(notificationsCountElem.data('count'));
            var notifications          = notificationsWrapper.find('ul.menu');
            var header                 = notificationsWrapper.find('.header');

            // var messageWrapper   = $('.messages-menu');
            // var messageToggle    = messageWrapper.find('a[data-toggle]');
            // var messageCountElem = messageToggle.find('span[data-count]');
            // var messageCount     = parseInt(messageCountElem.data('count'));
            // var message          = messageWrapper.find('ul.menu');
            // var msgheader                 = messageWrapper.find('.header');


            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;
            var key = '{{env('PUSHER_APP_KEY')}}' ;
// console.log(key);
            var pusher = new Pusher(key, {
                cluster: 'mt1',
                encrypted: true
            });

            // Subscribe to the channel we specified in our Laravel Event
            var notifychannel = pusher.subscribe('Notify');
            var messagechannel = pusher.subscribe('Chat');

            // Bind a function to a Event (the full Laravel class)
            notifychannel.bind('send-message-{{\Auth::id()}}', function(data) {
                var existingNotifications = notifications.html();
                var newNotificationHtml = "<li><!-- start message -->\n" +
                    "                <a href=\"/admin/user/profile\">\n" +
                    "                    <div class=\"pull-left\">\n" +
                    "                        <img src='"+ data.avatar +"' class='img-circle' style='width: 35px; height: 35px' alt='User Image'>\n" +
                    "                    </div>\n" +
                    "                    <h4>" + data.from_name +
                    "                        <small><i class='fa fa-clock-o'></i> "+ data.time + "</small>\n" +
                    "                    </h4>\n" +
                    "                    <p>"+ data.content + "</p>" +
                    "                </a>" +
                    "            </li>";
                notifications.html(newNotificationHtml + existingNotifications);

                notificationsCount += 1;
                notificationsCountElem.attr('data-count', notificationsCount);
                header.find('span').text(notificationsCount);
                notificationsCountElem.text(notificationsCount);
                notificationsWrapper.find('.notif-count').text(notificationsCount);
                notificationsWrapper.show();
            });

            {{--// Bind a function to a Event (the full Laravel class)--}}
            {{--messagechannel.bind('send-message-{{\Auth::id()}}', function(data) {--}}
            {{--    var existingMessage = message.html();--}}
            {{--    var newNotificationHtml = "<li><!-- start message -->\n" +--}}
            {{--        "                <a href=\"/admin/message\">\n" +--}}
            {{--        "                    <div class=\"pull-left\">\n" +--}}
            {{--        "                        <img src='"+ data.avatar +"' class='img-circle' style='width: 35px; height: 35px' alt='User Image'>\n" +--}}
            {{--        "                    </div>\n" +--}}
            {{--        "                    <h4>" + data.from_name +--}}
            {{--        "                        <small><i class='fa fa-clock-o'></i> "+ data.time + "</small>\n" +--}}
            {{--        "                    </h4>\n" +--}}
            {{--        "                    <p>"+ data.message + "</p>" +--}}
            {{--        "                </a>" +--}}
            {{--        "            </li>";--}}
            {{--    message.html(newNotificationHtml + existingMessage);--}}

            {{--    messageCount += 1;--}}
            {{--    messageCountElem.attr('data-count', messageCount);--}}
            {{--    msgheader.find('span').text(messageCount);--}}
            {{--    messageCountElem.text(messageCount);--}}
            {{--    messageWrapper.find('.notif-count').text(messageCount);--}}
            {{--    messageWrapper.show();--}}
            {{--});--}}
        })
    </script>
    @stack('js')
    @yield('js')
@stop
