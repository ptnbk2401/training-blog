@extends('adminlte::page')

@section('css')
    <style>
        #change_avatar {
            border: 1px dashed #89aef1;
            padding: 2px;
            width: 70%;
            margin: 0 auto;
            text-align: center;
        }

    </style>
@stop
@section('title', 'AdminLTE - Profile')

@section('content_header')
    <h1>Message</h1>
@stop

@section('content')
@php

@endphp
    <div class="row">
        <div class="col-md-6">
            <!-- DIRECT CHAT -->
            <div class="box box-warning direct-chat direct-chat-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Group Chat</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts"
                                data-widget="chat-pane-toggle">
                            <i class="fa fa-comments"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages" id="chatBox">
                    @php
                        Carbon\Carbon::setLocale('vi');
                        $now     = Carbon\Carbon::now();
                    @endphp
                    @for($key = count($messages)-1; $key >=0 ;  $key--)
                        @php
                            $message = $messages{$key};
                            $user = $message->user;
                            if(!empty($user->avatar)) {
                                $hinhanh = $user->avatar->avatar;
                                $path = storage_path('app/public/media/files/users/' .$user->avatar->avatar) ;
                                if( !empty( $hinhanh ) && file_exists( $path ) ) {
                                  $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'users', 40, 40) ;
                                } else {
                                  $anh = '/templates/core/images/avatar-profile.png';
                                }
                            }
                            else {
                                $anh = '/templates/core/images/avatar-profile.png';
                            }
                        @endphp

                        <!-- Message. Default to the left -->
                            <div class="direct-chat-msg {{ \Auth::id()!=$message->user_id ? '' : 'right'}}" >
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-{{\Auth::id()!=$message->user_id ? 'left' : 'right'}}">{{ $user->name }}</span>
                                    @php
                                        $dt = Carbon\Carbon::createFromTimeString($message->created_at);
                                    @endphp
                                    @if ( $now->diffInHours($dt) <= 23 )
                                        <span class="direct-chat-timestamp pull-{{\Auth::id()!=$message->user_id ? 'right' : 'left'}}">{{ $dt->diffForHumans($now) }}</span>
                                    @else
                                        <span class="direct-chat-timestamp pull-{{\Auth::id()!=$message->user_id ? 'right' : 'left'}}">{{ date('d M, H:i',strtotime($dt)) }}</span>
                                    @endif
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="{{ $anh }}" alt="message user image">
                                <!-- /.direct-chat-img -->
                                <div class="direct-chat-text" style="text-align: {{\Auth::id()!=$message->user_id ? 'left' : 'right'}}">
                                    {{ $message->message }}
                                </div>
                                @if($key==count($messages)-1)
                                    <input type="text" autofocus style="display: none">
                                @endif
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->

                        @endfor


                    </div>
                    <!--/.direct-chat-messages-->

                    <!-- Contacts are loaded here -->
                    <div class="direct-chat-contacts">
                        <ul class="contacts-list">

                            <!-- End Contact Item -->
                        </ul>
                        <!-- /.contatcts-list -->
                    </div>
                    <!-- /.direct-chat-pane -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <form method="post" action="javascript:;" onsubmit="SubmitAjax()" id="send_message" >
                        <div class="input-group">
                            <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat">Send</button>
                          </span>
                        </div>
                    </form>
                </div>
                <!-- /.box-footer-->
            </div>
            <!--/.direct-chat -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

@stop
@section('js')
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <script>
        function SubmitAjax(){
            var message = $('input[name=message]').val();
            if (ajax_sendding == true){
                return false;
            }
            ajax_sendding = true;
            $.ajax({
                url: "{{ route('user.postmessage') }}",
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                type: 'POST',
                cache: false,
                processData: false,
                contentType: false,
                data: new FormData($("#send_message")[0]),
                success: function(data){
                    chatBox = $( "#chatBox" );
                    // $(chatBox).append(data);
                    gotoBottom()
                },
                error: function (){
                    alert('Có lỗi xảy ra');
                }
            }).always(function(){
                ajax_sendding = false;
                $('input[name=message]').val('');
            });
            return false;
        }
    </script>
    <script>
        $(document).ready(function(){
            gotoBottom();
            ajax_sendding = false;
        });
        function gotoBottom() {
            $('#chatBox').animate({ scrollTop: $('#chatBox')[0].scrollHeight}, 2000);
        }
        $(function () {
           // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;
            var key = '{{env('PUSHER_APP_KEY')}}' ;
            var pusher = new Pusher(key, {
                cluster: 'mt1',
                encrypted: true
            });

            // Subscribe to the channel we specified in our Laravel Event
            var messagechannel = pusher.subscribe('MessageSent');

            // Bind a function to a Event (the full Laravel class)
            messagechannel.bind('chat', function(data) {
                var user_id = {{ \Auth::id() }};
                var newNotificationHtml = "";

                if(user_id != data.user_id) {
                    newNotificationHtml += '<div class="direct-chat-msg " >';
                    newNotificationHtml +='<div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">'+ data.from_name +'</span>';
                    newNotificationHtml +='<span class="direct-chat-timestamp pull-right">'+ data.time +'</span>';
                    newNotificationHtml +='</div>';
                    newNotificationHtml +='<img class="direct-chat-img" src="'+data.avatar+'" alt="message user image">';
                    newNotificationHtml +='<div class="direct-chat-text" style="text-align: left; border-color: #d6ac1b; border-left-color: #f39c12;" >'+data.message+'</div></div>';
                } else {
                    newNotificationHtml += '<div class="direct-chat-msg right" >';
                    newNotificationHtml +='<div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">'+ data.from_name +'</span>';
                    newNotificationHtml +='<span class="direct-chat-timestamp pull-left">'+ data.time +'</span>';
                    newNotificationHtml +='</div>';
                    newNotificationHtml +='<img class="direct-chat-img" src="'+data.avatar+'" alt="message user image">';
                    newNotificationHtml +='<div class="direct-chat-text" style="text-align: right ; background: #0d6aad"  >'+data.message+'</div></div>';
                }


                chatBox = $( "#chatBox" );
                $(chatBox).append(newNotificationHtml);
                gotoBottom()

            });
        })

    </script>
@stop
