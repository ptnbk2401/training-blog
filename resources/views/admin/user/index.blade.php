@extends('adminlte::page')

@section('title', 'AdminLTE')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>

</style>
@stop
@section('content_header')
    <h1>Users - Laravel</h1>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div id="app">
            <main-component></main-component>
        </div>
    <!-- /.box -->
    </div>
</div>
@stop
@section('js')
<script>
		$('#groups').select2({
	      placeholder: 'Chọn nhóm',
	    });
</script>
<script src="/js/app.js"></script>

@stop