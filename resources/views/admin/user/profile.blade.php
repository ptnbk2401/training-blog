@extends('adminlte::page')

@section('css')
    <style>
        #change_avatar {
            border: 1px dashed #89aef1;
            padding: 2px;
            width: 70%;
            margin: 0 auto;
            text-align: center;
        }

    </style>
@stop
@section('title', 'AdminLTE - Profile')

@section('content_header')
    <h1>Trang cá nhân</h1>
@stop

@section('content')
@php
    if( !empty( $user->avatar )  ) {
        $hinhanh = $user->avatar->avatar;
        $path = storage_path('app/public/media/files/users/' .$user->avatar->avatar) ;
        if( !empty( $hinhanh ) && file_exists( $path ) ) {
          $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'users', 88, 88) ;
        } else {
          $anh = '/templates/core/images/avatar-profile.png';
        }
    }
    else {
      $anh = '/templates/core/images/avatar-profile.png';
    }
    $profile = $user->profile;
@endphp
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{$anh}}"
                         alt="User profile picture" id="anh-avatar">

                    <h3 class="profile-username text-center">{{$user->name}}</h3>

                    <p class="text-muted text-center">{{ $user->groups[0]->gname }}</p>

                    <ul class="list-group list-group-unbordered">
                    </ul>
                    <form id="change_avatar" method="post" enctype="multipart/form-data">
                        <label class="custom-file-upload" >
                            <input type="file" name="avatar" id="avatar-upload" style="display: none;">
                            <i class="fa fa-upload" aria-hidden="true"></i> Change Avatar
                        </label>
                    </form>

{{--                    <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>--}}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">About Me</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

                    <p class="text-muted">
                        {{ !empty($profile->education)? $profile->education : ''  }}
                    </p>

                    <hr>

                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                    <p class="text-muted">{{ !empty($profile->location)? $profile->location : ''  }}</p>

                    <hr>

                    <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                    @php
                        $skills =  !empty($profile->skills)? $profile->skills : [] ;
                        $arLable = ['danger','success','info','warning','primary','danger','success','info','warning','primary','danger','success','info','warning','primary'];
                    @endphp
                    <p>
                        @if(!empty($skills))
                            @foreach(explode('|', $skills) as $index=>$skill)
                                <span class="label label-{{$arLable[$index]}}">{{$skill}}</span>
                            @endforeach
                        @endif
                    </p>

                    <hr>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                    <p>{!! !empty($profile->notes)? $profile->notes : ''  !!}.</p>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
{{--                    <li><a href="#timeline" data-toggle="tab">Timeline</a></li>--}}
                    <li><a href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                    @php
                        Carbon\Carbon::setLocale('vi');
                        $now     = Carbon\Carbon::now();
                    @endphp
                        @if(!empty($ratings))
                            @foreach($ratings as $rating)
                                <!-- Post -->
                                    @php
                                            $user_rating = \App\User::find($rating->user_id);
                                            $avatar = !empty($user_rating->avatar)? asset("/storage/media/files/users/{$user_rating->avatar->avatar}")  : '/templates/core/images/avatar-profile.png' ;
                                    @endphp
                                    <div class="post">
                                        <div class="user-block">
                                            <img class="img-circle img-bordered-sm avatar-user{{$user_rating->id}}" src="{{$avatar}}"
                                                 alt="user image" >
                                            <span class="username">
                                              <a href="#">{{ $user_rating->name }}</a>
                                              <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                            </span>
                                            <span class="description">
                                                @php
                                                    $dt = Carbon\Carbon::createFromTimeString($rating->created_at);
                                                @endphp
                                                @if ( $now->diffInHours($dt) <= 23 )
                                                    {{ $dt->diffForHumans($now) }}
                                                @else
                                                    {{ date('d/m/Y',strtotime($dt)) }}
                                                @endif
                                            </span>
                                        </div>
                                        <!-- /.user-block -->
                                        <a href="javascript:;"><b style="color: #1b6d85">{{ $rating->title }}</b></a>
                                        <p>
                                            Đã đánh giá bài viết <b style="color: mediumvioletred">{{ $rating->rating }}</b> Sao
                                        </p>

                                    </div>
                            @endforeach
                            <ul class="pagination pagination-small">
                                {{ $ratings->links() }}
                            </ul>
                        @endif
                        <!-- /.post -->
                    </div>

                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="settings">
                        <form class="form-horizontal" role="form" id="profile" action="{{ route('user.profile',[\Auth::id()]) }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Name</label>

                                <div class="col-sm-10">
                                    <input type="text" name="name" value="{{ $user->name }}" class="form-control" id="inputName" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-10">
                                    <input type="email" name="email" value="{{ $user->email }}" readonly class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Gender</label>

                                <div class="col-sm-10">
                                    @php
                                        $arGender = ['male','female'];
                                        $oldgender = !empty($profile->gender) ? $profile->gender : '';
                                    @endphp
                                    <select name="gender" id="inputGender" class="form-control" required>
                                        <option value=""> -- Select One --</option>
                                        @foreach( $arGender  as $gender)
                                            @php
                                                $selected = ( $oldgender == $gender )? 'selected' : '';
                                            @endphp
                                            <option {{ $selected }} value="{{$gender}}"> {{$gender}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputExperience" class="col-sm-2 control-label">Education</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" required name="education" id="inputEducation" placeholder="Education">{{ !empty($profile->education) ? $profile->education : '' }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputLocation" class="col-sm-2 control-label">Location</label>

                                <div class="col-sm-10">
                                    <input type="text"  name="location" required value="{{ !empty($profile->location) ? $profile->location : '' }}" class="form-control" id="inputLocation" placeholder="Location">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputSkills" class="col-sm-2 control-label">Skills</label>
                                @php
                                    $skill_old = !empty($profile->skills) ? explode('|',$profile->skills) : [];
                                    $arSkills = ['UI Design', 'Javascript', 'PHP', 'Node.js'];
                                @endphp
                                <div class="col-sm-10 col-md-10 ">
                                    <select name="skills[]" id="skilss" required class="form-control" multiple style="width: 100%">
                                        @foreach ($arSkills as $skill)
                                            @php
                                                $selected = in_array($skill,$skill_old) ? 'selected' : '';
                                            @endphp
                                            <option {{ $selected }} value="{{ $skill }}">{{ $skill }}</option>
                                        @endforeach
                                        @foreach($skill_old as $skill)
                                            @php
                                                $selected = !in_array($skill,$arSkills) ? 'selected' : '';
                                            @endphp
                                            <option {{ $selected }} value="{{ $skill }}">{{ $skill }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputNotes" class="col-sm-2 control-label">Notes</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" required name="notes" id="inputNotes" placeholder="Notes">{{ !empty($profile->notes) ? $profile->notes : '' }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" form="profile" class="btn btn-danger">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

@stop
@section('js')
    <script>
        $(function () {
            $('#cat_id').select2({
                placeholder: 'Chọn danh mục',
            });

            $('#skilss').select2({
                placeholder: 'Nhập thẻ Skill',
                tags: true,
                tokenSeparators: [',', ';'],
            });
            ajax_sendding = false;
            $.ajaxSetup({
                headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
            });
        });

        $("#avatar-upload").change(function () {
            // $('#avatar-user').html('<i class="fa fa-fw fa-spinner" style="font-size: 20px; color: blue"></i>');
            $.ajax({
                url: "{{ route('user.avatar') }}",
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                type: 'POST',
                cache: false,
                processData: false,
                contentType: false,
                data: new FormData($("#change_avatar")[0]),
                success: function(data){
                   if(data!=0) {
                        $('#anh-avatar').attr('src',data);
                        $('.avatar-user{{\Auth::id()}}').attr('src',data);
                   }
                },
                error: function (){
                    alert('Có lỗi xảy ra');
                }
            });
            return false;
            // alert(tags);
        });

    </script>
@stop
