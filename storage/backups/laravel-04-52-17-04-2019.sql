-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: laravel
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Giải Trí',100,NULL,NULL),(2,'Thời Sự',100,NULL,NULL),(3,'Thể Thao',100,NULL,NULL),(4,'Thời tiết',99,NULL,NULL),(5,'Văn hóa',99,NULL,NULL),(6,'Game',99,NULL,NULL),(7,'Teen',99,NULL,NULL),(8,'Nhịp sống trẻ',99,NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin',NULL,NULL),(2,'editor',NULL,NULL),(3,'user',NULL,NULL),(4,'Người lạ ơi',NULL,NULL);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (5,'default','{\"displayName\":\"App\\\\Jobs\\\\SendReminderEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendReminderEmail\",\"command\":\"O:26:\\\"App\\\\Jobs\\\\SendReminderEmail\\\":7:{s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";i:15;s:7:\\\"chained\\\";a:0:{}}\"}}',5,1555409025,1555409024,1555409024);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_04_04_015345_create_groups_table',1),(4,'2019_04_04_015808_create_user_group_table',1),(5,'2019_04_04_074231_create_tags_table',1),(6,'2019_04_04_074709_create_post_category_table',1),(7,'2019_04_04_075319_create_post_tag_table',1),(8,'2019_04_04_080110_create_categories_table',1),(9,'2019_04_04_080444_create_posts_table',1),(10,'2019_04_05_094422_create_user_post_table',1),(11,'2014_10_12_000000_create_users_table',2),(12,'2019_04_16_075237_create_jobs_table',3),(13,'2019_04_16_090411_create_jobs_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_category`
--

DROP TABLE IF EXISTS `post_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_category`
--

LOCK TABLES `post_category` WRITE;
/*!40000 ALTER TABLE `post_category` DISABLE KEYS */;
INSERT INTO `post_category` VALUES (32,1,1,NULL,NULL),(33,2,1,NULL,NULL),(34,3,1,NULL,NULL),(35,4,1,NULL,NULL),(36,5,1,NULL,NULL),(37,6,2,NULL,NULL),(38,7,2,NULL,NULL),(39,8,2,NULL,NULL),(40,9,2,NULL,NULL),(42,11,2,NULL,NULL),(43,12,3,NULL,NULL),(44,13,3,NULL,NULL),(45,14,3,NULL,NULL),(46,15,3,NULL,NULL),(47,16,3,NULL,NULL),(48,17,3,NULL,NULL),(49,18,3,NULL,NULL),(50,19,3,NULL,NULL),(51,20,3,NULL,NULL),(52,21,2,NULL,NULL),(53,22,3,NULL,NULL),(54,23,2,NULL,NULL),(55,24,2,NULL,NULL),(56,25,2,NULL,NULL),(57,26,3,NULL,NULL),(58,27,3,NULL,NULL),(59,28,3,NULL,NULL),(60,29,3,NULL,NULL),(61,30,3,NULL,NULL),(62,31,1,NULL,NULL),(63,32,1,NULL,NULL),(64,33,1,NULL,NULL),(65,34,1,NULL,NULL),(66,35,1,NULL,NULL),(67,36,2,NULL,NULL),(68,37,2,NULL,NULL),(69,38,2,NULL,NULL),(70,39,2,NULL,NULL),(71,40,2,NULL,NULL),(82,10,3,NULL,NULL),(83,10,2,NULL,NULL),(90,44,1,NULL,NULL),(91,43,1,NULL,NULL),(92,42,1,NULL,NULL),(93,41,1,NULL,NULL),(94,50,2,NULL,NULL),(95,50,8,NULL,NULL),(96,50,7,NULL,NULL),(97,50,6,NULL,NULL),(98,49,2,NULL,NULL),(99,49,6,NULL,NULL),(100,49,5,NULL,NULL),(101,45,1,NULL,NULL),(102,45,6,NULL,NULL),(103,45,4,NULL,NULL),(104,48,3,NULL,NULL),(105,48,2,NULL,NULL),(106,48,1,NULL,NULL),(107,47,2,NULL,NULL),(108,47,1,NULL,NULL),(109,47,8,NULL,NULL),(110,46,2,NULL,NULL),(111,46,8,NULL,NULL),(112,46,5,NULL,NULL);
/*!40000 ALTER TABLE `post_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_tag`
--

DROP TABLE IF EXISTS `post_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_tag` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_tag`
--

LOCK TABLES `post_tag` WRITE;
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
INSERT INTO `post_tag` VALUES (1,30,1,NULL,NULL),(2,10,2,NULL,NULL),(3,10,3,NULL,NULL),(4,10,4,NULL,NULL),(22,44,20,NULL,NULL),(23,44,21,NULL,NULL),(24,44,22,NULL,NULL),(25,43,23,NULL,NULL),(26,43,24,NULL,NULL),(27,43,25,NULL,NULL),(28,42,26,NULL,NULL),(29,42,27,NULL,NULL),(30,42,28,NULL,NULL),(31,41,29,NULL,NULL),(32,41,30,NULL,NULL),(33,50,4,NULL,NULL),(34,50,3,NULL,NULL),(35,49,7,NULL,NULL),(36,49,6,NULL,NULL),(37,49,5,NULL,NULL),(38,45,19,NULL,NULL),(39,45,18,NULL,NULL),(40,45,17,NULL,NULL),(41,48,10,NULL,NULL),(42,48,9,NULL,NULL),(43,48,8,NULL,NULL),(44,47,13,NULL,NULL),(45,47,12,NULL,NULL),(46,47,11,NULL,NULL),(47,46,16,NULL,NULL),(48,46,15,NULL,NULL),(49,46,14,NULL,NULL);
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `preview_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Et tenetur necessitatibus voluptatem dolores. Numquam fuga et nostrum ut. Rerum eaque voluptate ut aperiam.','b68ca033453f2ca26e51f4750af9663a.jpg','Similique rerum sunt sit ab quia corporis rerum. Enim voluptatem eum blanditiis.',1,1,'Edwin and Morcar, the earls of Mercia and Northumbria, declared for him: and even Stigand, the patriotic archbishop of Canterbury, found it so yet,\' said the King, the Queen, and in another moment.','2019-04-15 23:47:52','2019-04-16 00:03:06'),(2,'Eos qui alias ea saepe dolorum ducimus beatae. Totam ut optio quas aut eum qui quia. Quis dolore molestias quis ut. Facere dolorem minima soluta consectetur.','6469554825e170a29fba48809581247a.jpg','Quis sit doloribus voluptatum sed tempora dolore aut. Et doloremque voluptatibus et dolorum veritatis. Eius et nam consequatur aperiam ut assumenda. Provident provident adipisci id a dolorem.',1,1,'YET,\' she said this, she was saying, and the shrill voice of thunder, and people began running when they hit her; and when she had to double themselves up and leave the court; but on second thoughts.','2019-04-15 23:47:56','2019-04-16 00:03:06'),(3,'Ducimus debitis nihil rem cum. Et et id nemo magnam eligendi suscipit. Id quam maxime aut ut qui. Enim assumenda omnis vel dignissimos rem ut.','53542328553e63b8076d6b123ffbedf0.jpg','Dolores sint est veniam. Aut quam qui eligendi aut quis.',1,1,'RED rose-tree, and we won\'t talk about trouble!\' said the Caterpillar. \'Well, perhaps not,\' said the Dodo, \'the best way you go,\' said the cook. \'Treacle,\' said a sleepy voice behind her. \'Collar.','2019-04-15 23:47:59','2019-04-16 00:03:05'),(4,'Qui officia modi nulla. Quia ipsa maxime adipisci voluptatum. Quis tenetur eius saepe quas consequatur sint.','61ab935b29403862408f08ade3956911.jpg','Sint velit voluptatum quaerat quam. Blanditiis blanditiis dolores nisi ut nisi eum aut nesciunt. Eligendi est aliquid necessitatibus culpa optio corrupti sit eaque.',1,1,'I was thinking I should be like then?\' And she kept on puzzling about it while the Dodo solemnly presented the thimble, saying \'We beg your pardon!\' said the Gryphon. \'I mean, what makes them so.','2019-04-15 23:48:00','2019-04-16 00:03:04'),(5,'Suscipit dicta omnis nulla dolore velit. Fuga provident veniam consequatur esse. Quae qui magnam debitis hic nihil quis ut quod. Est est neque porro culpa voluptatem et.','fee93c464a6d6e24f5bddd56988a4a76.jpg','Et atque est ut cupiditate. Incidunt expedita est ipsum quos rem eius quibusdam quam. Qui dolore veniam ut ea ut.',1,1,'Alice\'s shoulder as he spoke, and then dipped suddenly down, so suddenly that Alice said; \'there\'s a large pool all round the court and got behind him, and said to the Hatter. Alice felt dreadfully.','2019-04-15 23:48:01','2019-04-16 00:03:03'),(6,'Totam ab nisi molestiae. Dolores praesentium nemo qui ea. Ipsum in ut laboriosam ut. Et aliquam excepturi omnis aspernatur et tempora. Veritatis optio consequatur aperiam quisquam ex.','487ade21e60dcbbfc703fb59be48c479.jpg','Maiores eveniet ab et nulla ad explicabo pariatur sit. Dolorem minus repellat reiciendis. Numquam voluptate officiis ut qui quos hic quia.',1,1,'The Dormouse slowly opened his eyes. He looked anxiously at the Hatter, who turned pale and fidgeted. \'Give your evidence,\' the King said gravely, \'and go on crying in this affair, He trusts to you.','2019-04-15 23:48:03','2019-04-16 00:03:03'),(7,'Laudantium dolor omnis esse itaque a. Debitis vel non distinctio ut cum doloremque tenetur. Voluptates nobis recusandae dicta quia suscipit sed. Molestiae dolor nihil nulla odio dolores.','a906c9e5f9526c0c538b71d3dc77f09c.jpg','Eveniet aut quia dolor explicabo explicabo repellendus. Quasi et itaque quia exercitationem incidunt. Dolores est ipsum sed in. Aut dolor vero accusamus et libero.',1,1,'Hatter: and in another moment, splash! she was quite a conversation of it now in sight, and no one listening, this time, as it left no mark on the floor, and a large rabbit-hole under the sea,\' the.','2019-04-15 23:48:04','2019-04-16 00:03:02'),(8,'Veniam culpa nam modi vel voluptatibus. Iusto autem sunt repudiandae est animi ducimus voluptatum. Necessitatibus praesentium tempora dolorem quisquam.','cf7e861af2d30983ae5e9188eeb4486e.jpg','Saepe rerum maiores consequatur quia et ipsa. Voluptas accusamus temporibus quidem debitis quam. Voluptatibus maxime amet nostrum quia fuga consequatur in. Quos voluptate illo est.',1,1,'Do you think you\'re changed, do you?\' \'I\'m afraid I am, sir,\' said Alice; not that she ran out of breath, and till the Pigeon in a large rabbit-hole under the window, and on both sides at once. The.','2019-04-15 23:48:06','2019-04-16 00:03:00'),(9,'Harum repudiandae doloremque voluptas omnis ut et fuga. Et et commodi voluptas. Consequatur vel aspernatur rerum eos.','843fd2b91709840180b9dd621385fe02.jpg','Dolor doloribus corrupti est consectetur dicta non repellendus et. Qui culpa beatae reiciendis. Quos reprehenderit veritatis inventore ea laboriosam consequuntur molestiae.',1,1,'So Alice began in a loud, indignant voice, but she thought it over afterwards, it occurred to her that she was saying, and the King in a Little Bill It was so full of tears, \'I do wish they WOULD.','2019-04-15 23:48:07','2019-04-16 00:03:00'),(10,'Tempore qui quas ut itaque dolorem id. Dolores numquam alias inventore similique. Non fugiat voluptatem quae consequatur dolores omnis. Nulla deserunt assumenda iusto.','33e9b4fddba1029fe4a335e4c67c901f.jpg','Quia cum magni commodi libero sed. Dolores et et ullam velit placeat. Tenetur nulla illum eligendi.',1,1,'Twinkle, twinkle--\"\' Here the Queen said to the King, rubbing his hands; \'so now let the Dormouse fell asleep instantly, and neither of the words did not like to try the first figure,\' said the.','2019-04-15 23:48:08','2019-04-16 00:04:47'),(11,'Esse perferendis veritatis quisquam nesciunt est rerum quos. Adipisci quo harum qui expedita.','d1636ecee85ba8d4cffc58fb9d0eb7fd.jpg','Officiis eveniet nihil iusto consequatur omnis quo sed aut. Qui architecto alias modi voluptas et voluptatum dicta consequuntur. Neque quaerat molestiae magni maiores sed ipsum exercitationem.',1,1,'Dormouse. \'Write that down,\' the King said, with a trumpet in one hand and a Long Tale They were just beginning to see it trot away quietly into the darkness as hard as she remembered the number of.','2019-04-15 23:48:10','2019-04-16 00:03:50'),(12,'Quia perferendis totam aspernatur qui pariatur. Illum saepe error et quo tenetur et laboriosam commodi.','07e5148062994775989e40acc84af042.jpg','Porro earum adipisci cupiditate ut aut autem ut iure. Aliquam eos debitis atque ad ad. Aliquam quia omnis consectetur tenetur nostrum ipsa aut.',1,1,'Alice as he could go. Alice took up the chimney, and said nothing. \'This here young lady,\' said the Queen, pointing to the conclusion that it seemed quite dull and stupid for life to go on in the.','2019-04-15 23:48:11','2019-04-16 00:03:49'),(13,'Ipsa omnis et porro in architecto quis at. Minus molestias perferendis vitae nihil numquam illo. Et excepturi occaecati accusamus voluptatum distinctio. Aut omnis sint modi rem.','5cafda5110d7d24c0324a4bbddd93bea.jpg','Unde ullam fugit aut dicta eligendi. Excepturi qui alias et eligendi qui dolor. Ut ea sit iusto dolore magni voluptates. Dolorem error aut ex ducimus corporis et facere rem.',1,1,'However, \'jury-men\' would have called him a fish)--and rapped loudly at the end.\' \'If you didn\'t like cats.\' \'Not like cats!\' cried the Mouse, sharply and very soon finished off the top of the room.','2019-04-15 23:48:13','2019-04-16 00:03:48'),(14,'Alias libero voluptate accusamus pariatur asperiores. Ut aperiam saepe sunt iure qui aut labore possimus. Totam sunt dolorum et modi maxime. Voluptate maxime sed ipsa quis modi doloribus.','da2814cbd25a66ac3a7cd3a2cd662495.jpg','Doloremque mollitia reprehenderit ipsum. Et vitae aspernatur maxime incidunt ullam id porro. Voluptatibus quasi et officia ut ipsa voluptas ut voluptatem. Repudiandae fugiat non et.',1,1,'She got up and straightening itself out again, and did not feel encouraged to ask the question?\' said the Queen, who had been running half an hour or so there were no tears. \'If you\'re going to.','2019-04-15 23:48:14','2019-04-16 00:03:47'),(15,'Nulla beatae ipsam voluptatem similique. Quisquam laborum rerum ut rerum quis eum eum. Natus eveniet vel deleniti et cum vero.','555334e4cbd517078acb74d3e4f55fb2.jpg','Natus magni deleniti aut est quasi et. Doloribus inventore autem aperiam illum molestias quos. Quasi laboriosam voluptatem aliquid excepturi libero. Quaerat aut deserunt ipsum deleniti.',1,1,'Gryphon, \'that they WOULD put their heads down! I am very tired of being such a capital one for catching mice you can\'t swim, can you?\' he added, turning to Alice, very loudly and decidedly, and the.','2019-04-15 23:48:16','2019-04-16 00:03:47'),(16,'Nemo vitae ut consequatur omnis. Et dolores corrupti ducimus ut. Fugit modi porro ut enim a vel.','bfb374351c2dc0954520eb1ff8305659.jpg','Ratione vitae culpa repellat voluptatibus. Porro omnis temporibus repellat labore explicabo ab.',1,1,'WOULD always get into that lovely garden. I think you\'d take a fancy to cats if you only walk long enough.\' Alice felt so desperate that she had read about them in books, and she dropped it hastily.','2019-04-15 23:48:17','2019-04-16 00:03:45'),(17,'Totam provident cumque sunt dicta est possimus sapiente. Blanditiis repellendus et vero. Perferendis voluptas voluptatum et sed sit et quaerat.','817e9afc47f2631a776225d4a047bc6f.jpg','Fugit in voluptate impedit temporibus sed. Nihil temporibus velit dolor sunt est aut. Quia voluptatem modi tempora nostrum quia nam.',1,1,'French lesson-book. The Mouse looked at it gloomily: then he dipped it into one of the room. The cook threw a frying-pan after her as she could, for her to begin.\' He looked anxiously over his.','2019-04-15 23:48:18','2019-04-16 00:03:45'),(18,'Odit repellat aliquam eius qui. Eveniet ipsa molestias labore amet.','23da7ca45dd50cb403599bde80e393e4.jpg','Nihil et molestias nihil maxime. Ut dolores necessitatibus corrupti est laudantium delectus. Deserunt numquam ducimus suscipit quis quasi.',1,1,'WOULD not remember the simple rules their friends had taught them: such as, \'Sure, I don\'t want YOU with us!\"\' \'They were learning to draw, you know--\' \'What did they live at the top with its mouth.','2019-04-15 23:48:20','2019-04-16 00:03:43'),(19,'Nesciunt autem id ut deleniti. Velit sed aut recusandae quis corrupti facere. Sit expedita est aut recusandae. Ipsum est quos nostrum sint et magni.','1bcd80ec35c2b9db098c1f148c599b4a.jpg','Exercitationem cumque accusamus laborum dignissimos. Delectus officia quasi neque. Et doloribus voluptas molestiae sint omnis molestiae.',1,1,'BEST butter, you know.\' \'Who is this?\' She said it to her in a hurry. \'No, I\'ll look first,\' she said, \'for her hair goes in such confusion that she was ever to get out again. The Mock Turtle a.','2019-04-15 23:48:22','2019-04-16 00:03:43'),(20,'Deleniti tempore et dolor est. Debitis sed eius nemo nihil quis necessitatibus dolor. Qui et rerum possimus laudantium id aspernatur debitis laudantium.','a5017a8b99cad5c74c544ca8b3866b3b.jpg','In eos ut assumenda dolorem aut libero sit. Qui et dolores alias.',1,1,'Alice remarked. \'Oh, you can\'t swim, can you?\' he added, turning to Alice, and sighing. \'It IS a Caucus-race?\' said Alice; not that she did not get hold of it; so, after hunting all about it!\' and.','2019-04-15 23:48:23','2019-04-16 00:03:42'),(21,'Rerum libero minima aperiam ut. Sed et occaecati officiis repellendus. Aut blanditiis est fuga est laudantium repellat velit.','8911d959242f7a02026b85d813e66da8.jpg','Unde deserunt suscipit ut velit ut et perferendis. Dolorum sit harum repudiandae debitis. Quasi ullam id minus consequatur dolor molestiae aut.',1,1,'Queen to-day?\' \'I should think it would not allow without knowing how old it was, even before she came rather late, and the Panther received knife and fork with a sudden leap out of court! Suppress.','2019-04-15 23:48:24','2019-04-16 00:03:39'),(22,'Non vitae molestias ullam perferendis nostrum ut soluta. Labore aliquam magni provident tempore et eos vitae. Qui blanditiis autem occaecati voluptatum.','fe7e8fbd495538637ea5c5776927353c.jpg','Ex error blanditiis molestias molestiae. A dolore exercitationem quaerat nostrum quis molestiae nisi. Nostrum perferendis sed modi reiciendis inventore molestias et.',1,1,'King said, for about the games now.\' CHAPTER X. The Lobster Quadrille is!\' \'No, indeed,\' said Alice. \'It must be shutting up like a steam-engine when she next peeped out the Fish-Footman was gone.','2019-04-15 23:48:26','2019-04-16 00:03:37'),(23,'Dolor necessitatibus illo a minima sit sunt. Totam ea nostrum eum fugit aliquam. Ullam eveniet tempore eaque eligendi. Non illo dolorem enim quo.','602b624e5617ed7ff16b1aaa872f86c8.jpg','Nobis cupiditate quas ducimus tempore quidem. Est et ipsa possimus exercitationem nobis. Ipsum autem excepturi est ab vel debitis. Molestiae consectetur molestias ab aut.',1,1,'Let me see: that would happen: \'\"Miss Alice! Come here directly, and get ready for your interesting story,\' but she could not taste theirs, and the Mock Turtle. \'Seals, turtles, salmon, and so on.','2019-04-15 23:48:27','2019-04-16 00:03:36'),(24,'Repellendus illo est et nostrum et. Vero maiores et provident quam natus et. Aliquam dolorum qui a magnam quidem. Ut quia earum et ipsa odit.','d1ca650349da92b6c46555818d1b8a68.jpg','Beatae rerum numquam commodi omnis doloremque. Eaque et sit sit sed. Molestiae illum est eos impedit quia in asperiores.',1,1,'I could say if I fell off the fire, licking her paws and washing her face--and she is of finding morals in things!\' Alice thought this must ever be A secret, kept from all the first minute or two.','2019-04-15 23:48:28','2019-04-16 00:03:36'),(25,'Delectus rerum maxime enim quidem architecto odio. Et voluptatem qui autem accusantium nostrum cum. Fuga fugiat dolorem fugiat officiis veniam velit voluptas.','3993da5d8982c0ed77b3472d002c96bf.jpg','Et consequatur officia quae amet ut numquam ex. Voluptas quos rerum veniam architecto magni consequatur. Qui sapiente tempora quibusdam sit amet.',1,1,'I am very tired of this. I vote the young Crab, a little glass table. \'Now, I\'ll manage better this time,\' she said, by way of expecting nothing but a pack of cards: the Knave \'Turn them over!\' The.','2019-04-15 23:48:30','2019-04-16 00:03:35'),(26,'Iusto repellendus incidunt architecto atque autem omnis. Est quam est placeat ut quia non. Voluptatum iure est quaerat perspiciatis quam veritatis dolor.','2c447d3cfbbb041348db2bd5bd271ef0.jpg','Reprehenderit quia odio illum. Natus qui id adipisci non distinctio. Quia dolorem corporis impedit inventore et sed dolore fugiat. Non eum dolore rerum et cupiditate velit.',1,1,'I ever saw in my own tears! That WILL be a book written about me, that there was Mystery,\' the Mock Turtle persisted. \'How COULD he turn them out with trying, the poor animal\'s feelings. \'I quite.','2019-04-15 23:48:31','2019-04-16 00:03:35'),(27,'Distinctio ipsum corrupti delectus blanditiis. Officiis pariatur perferendis in. Est perspiciatis sed dolores soluta.','c361bba4dc1efccfa83a667f0ca97eef.jpg','Ab nam facilis minus occaecati voluptatem quas. Rerum voluptas eius omnis est modi soluta. Pariatur non quia quidem et incidunt qui doloremque ducimus.',1,1,'Lory, with a sudden leap out of the others looked round also, and all the party sat silent for a good deal frightened by this time?\' she said to herself, \'it would be quite as much as she left her.','2019-04-15 23:48:32','2019-04-16 00:03:33'),(28,'Ipsam cupiditate magnam earum voluptas. Quo ut autem est error illum debitis. Voluptatem consequatur quidem rerum animi aliquid.','23188ac46ea4c09c3510edfd6e7b99ae.jpg','Quis libero est nemo et. Debitis reiciendis accusantium et placeat. Voluptatem veniam veniam id non voluptas.',1,1,'Queen?\' said the Gryphon. \'I mean, what makes them so shiny?\' Alice looked very anxiously into its face to see the earth takes twenty-four hours to turn into a graceful zigzag, and was coming to.','2019-04-15 23:48:33','2019-04-16 00:03:33'),(29,'Quo occaecati facere a iure similique temporibus esse. Repudiandae amet voluptatem voluptates vitae inventore. Cum et quae necessitatibus ex quisquam vel dolor.','7293785236666c982d8f4b9e086787fe.jpg','Sint dolores voluptates aut sed. Molestias accusamus placeat dolore vero ea quae. Velit vitae accusantium doloremque est tempora.',1,1,'Alice. \'Why?\' \'IT DOES THE BOOTS AND SHOES.\' the Gryphon at the proposal. \'Then the Dormouse denied nothing, being fast asleep. \'After that,\' continued the Gryphon. Alice did not answer, so Alice.','2019-04-15 23:48:35','2019-04-16 00:03:32'),(30,'Quis autem aperiam et nulla id et sapiente. Voluptates et qui porro earum quo impedit vel illo.','b6179806292c10cb87fe398a5e3d3d41.jpg','Consequatur ducimus sed eos perferendis enim. Aliquam et nisi nisi nihil culpa. Maiores distinctio voluptatem sequi. Modi suscipit maiores soluta quidem impedit ut.',1,1,'I to do?\' said Alice. \'Of course twinkling begins with an M, such as mouse-traps, and the blades of grass, but she could guess, she was as much use in waiting by the White Rabbit, who said in an.','2019-04-15 23:48:36','2019-04-16 00:03:31'),(31,'Laborum deserunt dolorum non cum. Vel sint vero ab ipsa sunt libero dolorem. Dolores aut dolorum tenetur. Voluptatum et nihil ea nesciunt accusantium provident reiciendis rerum.','744c418b1aafb00083ce2aaf7cffe77b.jpg','Sit voluptatum quis odio velit repellat veritatis. Quae excepturi et aut doloremque sit ullam. Fugit expedita culpa aut ipsum fuga quia qui.',1,1,'Majesty,\' he began, \'for bringing these in: but I grow at a king,\' said Alice. \'I\'ve so often read in the pictures of him), while the rest of it now in sight, hurrying down it. There was a little.','2019-04-15 23:48:38','2019-04-16 00:03:27'),(32,'Explicabo cum et dolorum dolor quos vel consequatur. Dolor nesciunt iusto quo quo illo. Dolorum et amet qui. Eaque est et autem eveniet sit.','0c8024d29459400165a9273960ccc713.jpg','Blanditiis sequi rem officiis corporis corporis ullam. Est provident necessitatibus quo. Tempora eos voluptas magnam possimus omnis tenetur non. Qui quas nulla est quia officiis eius qui.',1,1,'Queen said severely \'Who is this?\' She said the Duchess; \'and the moral of that is, but I can\'t remember,\' said the Gryphon, sighing in his throat,\' said the Caterpillar; and it put more.','2019-04-15 23:48:39','2019-04-16 00:03:26'),(33,'Tempore perspiciatis praesentium ea asperiores soluta ipsum. Velit amet molestiae deserunt maxime a quia qui quae.','3e98c8c4b8e69b953f3919221cceb45c.jpg','Inventore dolores sed sunt porro. Est vel debitis unde aspernatur. Autem in id sit velit qui et.',1,1,'I can reach the key; and if the Mock Turtle. \'Very much indeed,\' said Alice. \'And ever since that,\' the Hatter said, turning to the shore. CHAPTER III. A Caucus-Race and a Canary called out \'The.','2019-04-15 23:48:40','2019-04-16 00:03:26'),(34,'Asperiores in expedita voluptate nobis. Suscipit omnis cupiditate soluta eaque aliquid perferendis ad. Est molestiae aliquid aliquam. Tenetur error labore rem fugit eos impedit maiores.','2ec5d423715656484eb7608028276d8d.jpg','Nulla distinctio est est omnis velit earum. Maiores facilis vero ab omnis. Eum nisi aliquid dolor fugiat odio voluptate. Culpa quisquam voluptatem vel dolor rerum quidem.',1,1,'Alice, \'it\'s very interesting. I never was so ordered about in the schoolroom, and though this was his first speech. \'You should learn not to her, And mentioned me to him: She gave me a pair of.','2019-04-15 23:48:42','2019-04-16 00:03:25'),(35,'Adipisci officia quidem cum quaerat id. Eos ex est aut sapiente doloribus. Et modi aut quod nemo dolor. Molestiae et consequuntur amet aut officiis quod.','35b334d2e94f05b8e50d0ac643d7c90b.jpg','Quam non esse odit voluptatem in amet pariatur. Magni sit sed vel deserunt quidem. Eos aut velit doloremque repudiandae dolores et.',1,1,'Lizard\'s slate-pencil, and the other end of the ground--and I should like it very nice, (it had, in fact, I didn\'t know it to be told so. \'It\'s really dreadful,\' she muttered to herself, \'in my.','2019-04-15 23:48:43','2019-04-16 00:03:24'),(36,'Quisquam qui voluptatibus et a. Illum culpa accusamus minus dolores qui dolor alias. Fuga libero ipsa est cumque. Rerum beatae maxime atque.','7814fcf25ccc2ff40db799d2ad1c7bc0.jpg','Sunt assumenda quia ut corrupti. Omnis sed et dolor rerum autem sit. Architecto ea qui eos dolores commodi dolorem. Qui quasi fugiat nostrum et illum est voluptatem.',1,1,'ALL RETURNED FROM HIM TO YOU,\"\' said Alice. \'Come, let\'s try the experiment?\' \'HE might bite,\' Alice cautiously replied, not feeling at all what had become of you? I gave her one, they gave him two.','2019-04-15 23:48:44','2019-04-16 00:03:24'),(37,'Sint dolores et rem aspernatur assumenda sit id. Dolorem eos ea eius nisi ut. Qui nam et ut neque sed maxime.','72e8548955f86f7d9f55fae48a438507.jpg','Ut repudiandae cumque maxime nobis et. Adipisci dolor neque debitis at vitae vel dolore. Sed et totam rerum accusantium. In reprehenderit molestiae laboriosam rerum.',1,1,'Rabbit\'s little white kid gloves in one hand, and made another rush at the window, and on both sides of it, and then Alice dodged behind a great deal too flustered to tell them something more. \'You.','2019-04-15 23:48:45','2019-04-16 00:03:22'),(38,'Autem qui explicabo eius ad. Fuga soluta qui rerum et. Debitis laborum unde neque libero ut sapiente velit odio. Nesciunt corrupti ab nihil quidem.','8d09699803907bf9a426671659f1ff3c.jpg','Id ipsum modi velit et vero perferendis aliquid consequatur. Et fuga est voluptatibus et vitae repellendus. Quod et officiis dolorem ut est error eos.',1,1,'HER about it.\' (The jury all looked so good, that it was only a pack of cards, after all. I needn\'t be afraid of them!\' \'And who is to France-- Then turn not pale, beloved snail, but come and join.','2019-04-15 23:48:47','2019-04-16 00:03:22'),(39,'A veniam mollitia consequuntur quam aut. Sint voluptas ut ad saepe sit natus mollitia. Doloremque qui nulla impedit qui consequatur eius. Dolores at nihil eum ratione possimus perferendis velit.','3c552f6bc9594fa4cbf43e9775c54331.jpg','Dolor tenetur corrupti aut. Molestiae modi non porro commodi et nihil aut commodi. Sit facere reiciendis qui quia. Qui aperiam omnis voluptatibus aut voluptatem doloremque et quae.',1,1,'Hatter. \'He won\'t stand beating. Now, if you please! \"William the Conqueror, whose cause was favoured by the officers of the jurymen. \'No, they\'re not,\' said the Dormouse went on, taking first one.','2019-04-15 23:48:48','2019-04-16 00:03:21'),(40,'Quod a ipsa quis laboriosam ut. Debitis reprehenderit error aut cum totam dolorum architecto. Sint sint ratione eos. Velit quia voluptatum nobis vel.','fe731cdae0ba2ebef3de2a5ffe145eeb.jpg','Sequi eos qui sed et rerum voluptate. Quo aut quisquam tempore velit quam omnis omnis. Ex labore ullam hic. Omnis sequi expedita dicta quo qui.',1,1,'Be off, or I\'ll have you executed, whether you\'re a little animal (she couldn\'t guess of what work it would not allow without knowing how old it was, and, as the March Hare: she thought it had been.','2019-04-15 23:48:49','2019-04-16 00:03:20'),(41,'Tempora dolorem voluptas eius ipsum consectetur qui laboriosam. Dolorum soluta repellat voluptatum nisi voluptatem a voluptatem.','901909368baeac9e28e1849726b04d97.jpg','Dolore nihil tempora quae quibusdam. Porro consequatur nisi enim soluta maxime. Dicta vero repellendus quaerat qui ut eligendi repellat.',1,1,'SOMETHING interesting is sure to kill it in a tone of great dismay, and began by taking the little golden key in the beautiful garden, among the party. Some of the singers in the air: it puzzled her.','2019-04-15 23:48:51','2019-04-16 00:08:08'),(42,'Doloribus culpa voluptas quam culpa expedita explicabo pariatur. Eius aut excepturi laborum explicabo pariatur ea.','ccf5f3aeec49602f16f07d537c174e32.jpg','Repudiandae qui aspernatur nihil officia eius. Totam illo non repellat quidem est. Sint explicabo soluta alias quasi quo modi nihil.',1,1,'Magpie began wrapping itself up and down looking for eggs, I know THAT well enough; and what does it matter to me whether you\'re a little of the deepest contempt. \'I\'ve seen a good deal worse off.','2019-04-15 23:48:52','2019-04-16 00:07:51'),(43,'Quo est inventore totam nisi officia. Consequatur perferendis ipsum ullam sit asperiores. Quo voluptatem corrupti soluta omnis necessitatibus molestiae. Cumque porro magnam dolor iure.','f6de8d72963504a7865a6c17f433b7a7.jpg','Perspiciatis optio quod laborum maxime ex et. Eveniet occaecati et sed quis. Fuga autem ipsa cum est.',1,1,'As she said to herself; \'his eyes are so VERY tired of being such a long sleep you\'ve had!\' \'Oh, I\'ve had such a subject! Our family always HATED cats: nasty, low, vulgar things! Don\'t let him know.','2019-04-15 23:48:53','2019-04-16 00:07:34'),(44,'Ratione ut ea animi. Sunt dolorum itaque officia pariatur nulla reprehenderit voluptatum. Eaque quia aspernatur officia ipsa. Omnis beatae illum autem quia.','d97b8a70c0e8764928d45bee4fa4466e.jpg','Repellendus vel et totam id. Soluta unde molestias rem ipsa iure dolor. Laborum ratione rem consectetur sunt vel.',1,1,'Elsie, Lacie, and Tillie; and they lived at the March Hare. The Hatter was the first to speak. \'What size do you know about this business?\' the King very decidedly, and the Queen was close behind.','2019-04-15 23:48:54','2019-04-16 00:07:06'),(45,'Incidunt voluptatem est rem et. Voluptas culpa odio qui sed omnis voluptates sapiente. Consectetur exercitationem recusandae dignissimos culpa aut.','fc8a8d977ceacfc95a39bfa1b68de83d.jpg','Eos facilis quibusdam earum sed voluptatem sed minus qui. Dolorum qui ut ut alias nulla exercitationem voluptas. Asperiores rerum fugit soluta magnam ut.',1,1,'Edgar Atheling to meet William and offer him the crown. William\'s conduct at first was in livery: otherwise, judging by his garden, and marked, with one finger for the rest of the month is it?\' he.','2019-04-15 23:48:56','2019-04-16 00:15:26'),(46,'Facere cumque quia et vero. Perspiciatis totam eligendi cupiditate qui tempora aliquid. Ex necessitatibus aut necessitatibus. Impedit qui sequi harum voluptates sunt id.','05c0596448ee25c5a102338255c3e05c.jpg','Saepe at sapiente animi non harum aut nulla. Libero officia officia culpa eos ut id. Magnam eveniet ullam repellat ut error. Hic et earum autem.',1,1,'But, now that I\'m doubtful about the temper of your nose-- What made you so awfully clever?\' \'I have answered three questions, and that you have just been reading about; and when she noticed a.','2019-04-15 23:48:57','2019-04-16 00:15:59'),(47,'Soluta quo voluptas quasi voluptatem dolores sit animi. Ullam id eum voluptas in. Ut eaque illum explicabo sit doloremque consequatur. Voluptas qui at molestias dolore voluptate illum.','6beb4cdb42091f3874f454bb14619196.jpg','Consequatur dolor omnis quas et sit. Vel quisquam dolores perspiciatis veniam. Dolorum vel voluptatum reiciendis quos in.',1,1,'Who would not stoop? Soup of the evening, beautiful Soup! Beau--ootiful Soo--oop! Beau--ootiful Soo--oop! Beau--ootiful Soo--oop! Beau--ootiful Soo--oop! Beau--ootiful Soo--oop! Beau--ootiful.','2019-04-15 23:48:58','2019-04-16 00:15:45'),(48,'Quod quisquam sed vel et. Architecto omnis eum ratione id vel magni. Corrupti at officia repellat expedita est. Officia illum et omnis ut ducimus accusamus incidunt. Iusto excepturi omnis magni.','a9e9cdd9562e1241f7f16c70721273cd.jpg','Modi eum ut provident. Incidunt nobis dolores iusto mollitia dolor enim aut. Ipsam placeat voluptatem inventore occaecati nihil nulla praesentium deleniti. Commodi reiciendis voluptatem ut.',1,1,'King repeated angrily, \'or I\'ll have you executed on the same thing as \"I get what I was a good way off, and Alice looked at poor Alice, who felt ready to talk about her repeating \'YOU ARE OLD.','2019-04-15 23:49:00','2019-04-16 00:15:36'),(49,'Ea soluta id omnis. Non distinctio ut aut praesentium veritatis mollitia. Nostrum tempora quod nisi laborum sed. At ut iusto et eos nostrum excepturi sequi.','9f2e967ba055454ef936900d0de985f2.jpg','Tempora eaque omnis et sed. Distinctio architecto aperiam ullam quis. Earum id sequi deleniti necessitatibus corrupti et voluptatem. Aliquid corporis ut ut hic adipisci aut ut.',1,1,'I beat him when he sneezes: He only does it to speak again. In a little bit, and said \'That\'s very curious.\' \'It\'s all about for a rabbit! I suppose you\'ll be asleep again before it\'s done.\' \'Once.','2019-04-15 23:49:01','2019-04-16 00:15:16'),(50,'Vel sed laborum perspiciatis. Enim earum doloribus itaque sunt quia possimus.','49c3225e1ba01ebc1b65e96083502dd8.jpg','Perspiciatis necessitatibus provident et libero qui perspiciatis earum. Enim reprehenderit quasi illo placeat facilis impedit suscipit. Voluptatibus non ratione molestiae.',1,1,'Gryphon, \'that they WOULD put their heads downward! The Antipathies, I think--\' (she was obliged to say it any longer than that,\' said the others. \'Are their heads downward! The Antipathies, I.','2019-04-15 23:49:03','2019-04-16 00:15:05');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'12',NULL,NULL),(2,'Thể thao',NULL,NULL),(3,'việt nam',NULL,NULL),(4,'bóng đá',NULL,NULL),(5,'tin nóng',NULL,NULL),(6,'24h',NULL,NULL),(7,'biển đông',NULL,NULL),(8,'khỉ',NULL,NULL),(9,'vượng',NULL,NULL),(10,'đười ươi',NULL,NULL),(11,'hoa kỳ',NULL,NULL),(12,'trung quốc',NULL,NULL),(13,'thái lan',NULL,NULL),(14,'vịnh hạ long',NULL,NULL),(15,'phong nha',NULL,NULL),(16,'cố đô huế',NULL,NULL),(17,'cầu tràng tiền',NULL,NULL),(18,'cầu sông hàn',NULL,NULL),(19,'cầu rồng',NULL,NULL),(20,'thịt gà',NULL,NULL),(21,'thịt heo',NULL,NULL),(22,'thịt chó',NULL,NULL),(23,'sơn tùng',NULL,NULL),(24,'ngọc trinh',NULL,NULL),(25,'chipu',NULL,NULL),(26,'toán',NULL,NULL),(27,'lý',NULL,NULL),(28,'hóa',NULL,NULL),(29,'bóng chuyền',NULL,NULL),(30,'cầu lông',NULL,NULL);
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (1,1,1,NULL,NULL),(2,2,2,NULL,NULL),(3,3,3,NULL,NULL),(4,4,4,NULL,NULL),(5,5,3,NULL,NULL),(6,6,3,NULL,NULL);
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_post`
--

DROP TABLE IF EXISTS `user_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_post` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_post`
--

LOCK TABLES `user_post` WRITE;
/*!40000 ALTER TABLE `user_post` DISABLE KEYS */;
INSERT INTO `user_post` VALUES (1,1,1,NULL,NULL),(2,2,1,NULL,NULL),(3,3,1,NULL,NULL),(4,4,1,NULL,NULL),(5,5,1,NULL,NULL),(6,6,2,NULL,NULL),(7,7,2,NULL,NULL),(8,8,2,NULL,NULL),(9,9,2,NULL,NULL),(10,10,2,NULL,NULL),(11,11,2,NULL,NULL),(12,12,3,NULL,NULL),(13,13,3,NULL,NULL),(14,14,3,NULL,NULL),(15,15,3,NULL,NULL),(16,16,3,NULL,NULL),(17,17,3,NULL,NULL),(18,18,3,NULL,NULL),(19,19,3,NULL,NULL),(20,20,3,NULL,NULL),(21,1,2,NULL,NULL),(22,2,3,NULL,NULL),(23,3,2,NULL,NULL),(24,4,2,NULL,NULL),(25,5,2,NULL,NULL),(26,8,3,NULL,NULL),(27,9,3,NULL,NULL),(28,10,3,NULL,NULL),(29,11,3,NULL,NULL);
/*!40000 ALTER TABLE `user_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin Super','admin01@gmail.com',NULL,'$2y$10$EwGHnQ9P5iFe1yZ1398edut1Evu34oWoS7taU8P2hQWtpEeSpw3V.','',NULL,'2019-04-15 23:47:50','2019-04-15 23:47:50'),(2,'Ngọc Trinh','editor01@gmail.com',NULL,'$2y$10$WrnfBg9yxHDQ2wVrOYELHeeqyIBPg8kXTGdujgMGWD7yIjYayz4XC','',NULL,'2019-04-15 23:47:50','2019-04-15 23:47:50'),(3,'Sơn Tùng','user01@gmail.com',NULL,'$2y$10$hP0JlWXLAmA4VSEWPYSLdumbd6yDV6ngz5Y13u/V9Kumclo/VIZWi','',NULL,'2019-04-15 23:47:50','2019-04-15 23:47:50'),(4,'Somin','ptnbk2401@pt.com',NULL,'$2y$10$CZwW7D0hj6jCbJAek0.f8.IyN3ueGYTOS70cyO8uY9MLUpTW.IVm6','',NULL,'2019-04-16 00:17:47','2019-04-16 00:17:47'),(5,'Somin','ptnbk2401@outlook.com',NULL,'$2y$10$9Eyz7NRx4DumIsBroli8wOqg4AB4fW5cjtD.hjts1nr96lvEMtUta','','6g31DOI1tg2bgb8KKHaxvnYUVtuBjRRPNXkmAyuhtMVxpiY4N626ndvdp5xn','2019-04-16 00:31:43','2019-04-16 00:35:19'),(6,'Kwang Soo','kwangsoo@kw.cm',NULL,'$2y$10$LeXGt7bkEK6S4Ncf8vBW2uQKVjt/kvGJUAXYE/aO8g7fPnxsxfQnK','',NULL,'2019-04-16 18:14:39','2019-04-16 18:14:39');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-17 11:52:12
