<?php

namespace App\Jobs;

use App\Mail\SendMailable;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendReminderEmail  implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        Mail::to('ptnbk2401@outlook.com')->send(new SendMailable());
        $mailer->send('mail.reminder', [
            'user' => $this->user,
        ], function ($message) {
            $message->from('ptnbk2401@gmail.com', 'Remider');
            $message->to('ptnbk2401@outlook.com');
        });

    }
}
