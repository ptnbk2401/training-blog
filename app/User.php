<?php

namespace App;

use App\Message;
use App\Model\Notify\Notify;
use App\Model\Post\PostIndex;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /**
     * [groups description]
     * @return [type] [description]
     */
    public function groups()
    {
        return $this->belongsToMany('App\Model\User\GroupIndex', 'user_group', 'user_id', 'group_id');
    }
    /**
     * [posts description]
     * @return [type] [description]
     */
    public function posts()
    {
        return $this->hasMany('App\Model\Post\PostIndex','user_id');;
    }
    /**
     * A user can have many messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function isAdmin($id){
        $user = User::find($id);
        foreach ($user->groups as $group) {
            if($group->gname == 'admin') return 1;
        }
        return 0;
    }

    public function avatar()
    {
        return $this->hasOne('App\Model\Avatar\Avatar');
    }

    public function profile()
    {
        return $this->hasOne('App\Model\User\Profile');
    }

    public function notifies()
    {
        return $this->hasMany(Notify::class);
    }


    


}
