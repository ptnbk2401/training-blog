<?php

namespace App\Model\Avatar;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    protected $table = "avatars";
    protected $primaryKey = "id";
//    public    $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
