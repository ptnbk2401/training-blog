<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Profile extends Model
{
    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function getRanting($id){
        $result =  DB::table('ratings as r')
            ->select('r.*','p.title')
            ->join('posts as p','p.id','r.rateable_id')
            ->join('users as u','p.user_id','u.id')
            ->where('u.id',$id)
            ->orderBy('r.id','DESC')
            ->paginate(5);
            ;

        return $result;
    }
}
