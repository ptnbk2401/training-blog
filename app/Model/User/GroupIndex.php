<?php

namespace App\Model\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GroupIndex extends Model
{
    protected $table = "groups";
    protected $primaryKey = "id";
    public    $timestamps = false;

    protected $fillable = [
        'gname'
    ];
    public function getItems() {
        return GroupIndex::orderBy('id', 'DESC')->get();
    }

    public function addItem($arItem) {
        return GroupIndex::insert($arItem);
    }
    public function editItem($arItem,$id) {
        return GroupIndex::whereId($id)->update($arItem);
    }
    public function delItem($id) {
        return GroupIndex::whereId($id)->delete();
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_group','group_id', 'user_id');
    }

    public function hasAccess(array $permissions) : bool
    {
        foreach ($permissions as $permission) {
            if ($this->hasPermission($permission))
                return true;
        }
        return false;
    }

    private function hasPermission(string $permission) : bool
    {
        return $this->gname[$permission] ?? false;
    }
}
