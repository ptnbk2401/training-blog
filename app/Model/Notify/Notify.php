<?php

namespace App\Model\Notify;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    protected $table = "notifies";
    protected $primaryKey = "id";
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
