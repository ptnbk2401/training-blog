<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Users)
    {
        $this->Users = $Users;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $Users = $this->Users;
        $this->subject("HotMagazine - Báo cáo User!");
        return $this->view('mail.index',compact('Users'));
    }
}
