<?php

namespace App\Providers;

use App\Model\Post\PostIndex;
use App\Policies\PostPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        PostIndex::class => PostPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('manage-user', function ($user) {
            foreach ($user->groups as $group) {
                if ($group->gname == 'admin'){
                   return true;
                }
            }
           return false;
       });
    }
}
