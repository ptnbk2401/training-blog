<?php

namespace App\Widgets;

use App\Model\Post\PostIndex;
use Arrilot\Widgets\AbstractWidget;

class PopularIndex extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $objPost = PostIndex::where('status',1)->inRandomOrder()->limit(8)->get();
        return view('widgets.popular_index', [
            'config' => $this->config,
            'objPost' => $objPost,
        ]);
    }
}
