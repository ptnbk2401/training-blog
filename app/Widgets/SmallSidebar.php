<?php

namespace App\Widgets;

use App\Model\Category\Category;
use App\Model\Post\PostIndex;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\DB;

class SmallSidebar extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $objPost = PostIndex::where('status',1)
            ->leftjoin('ratings','posts.id','ratings.rateable_id')
            ->select('posts.*', DB::raw('AVG(rating) as avgrating'))
            ->orderBy('avgrating','DESC')
            ->groupBy('ratings.rateable_id')
            ->limit(6)->get();
//        dd($objPost);
        $objCat = Category::orderBy('sort','DESC')->orderBy('id','DESC')->limit(12)->get();
        return view('widgets.small_sidebar', [
            'config' => $this->config,
            'objPost' => $objPost,
            'objCat' => $objCat,
            
        ]);
    }
}
