<?php

namespace App\Widgets;

use App\Model\Post\PostIndex;
use Arrilot\Widgets\AbstractWidget;

class TopPostHeader extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        
        $objItems = PostIndex::where('status',1)->orderBy('id','DESC')->limit(6)->get();
        return view('widgets.top_post_header', [
            'config' => $this->config,
            'objItems' => $objItems,
        ]);
    }
}
