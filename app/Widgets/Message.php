<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\DB;

class Message extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $user = \Auth::user();
        $messages = DB::table('messages')
            ->select('messages.*','u.name')
            ->join('users as u','u.id','messages.user_id')
            ->where('user_id',$user->id)->limit(5)
            ->orderBy('messages.id', 'DESC')->get();
        return view('widgets.message', [
            'config' => $this->config,
            'messages' => $messages,
        ]);
    }
}
