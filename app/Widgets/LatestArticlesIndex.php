<?php

namespace App\Widgets;

use App\Model\Post\PostIndex;
use Arrilot\Widgets\AbstractWidget;

class LatestArticlesIndex extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $objPost = PostIndex::where('status',1)->orderBy('id','desc')->paginate(4);
        return view('widgets.latest_articles_index', [
            'config' => $this->config,
            'objPost' => $objPost,
        ]);
    }
}
