<?php

namespace App\Widgets;

use App\Model\Category\Category;
use Arrilot\Widgets\AbstractWidget;

class TopMenuHeader extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        
        $objCat = Category::orderBy('sort','DESC')->orderBy('id','DESC')->get();
        return view('widgets.top_menu_header', [
            'config' => $this->config,
            'objCat' => $objCat,
        ]);
    }
}
