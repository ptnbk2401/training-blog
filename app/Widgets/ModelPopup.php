<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class ModelPopup extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.model_popup', [
            'config' => $this->config,
        ]);
    }
}
