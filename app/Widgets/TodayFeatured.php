<?php

namespace App\Widgets;

use App\Model\Post\PostIndex;
use Arrilot\Widgets\AbstractWidget;

class TodayFeatured extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $from = date('Y-m-d').' 00:00:00';
        $to = date('Y-m-d').' 23:59:59';
        $objPost = PostIndex::where('status',1)->whereBetween('created_at', array($from, $to))->inRandomOrder()->limit(9)->get();
        return view('widgets.today_featured', [
            'config' => $this->config,
            'objPost' => $objPost,
        ]);
    }
}
