<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\DB;

class Notify extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $user = \Auth::user();
        $notifies = DB::table('notifies')
            ->select('notifies.*','u.name')
            ->join('users as u','u.id','notifies.user_id')
            ->where('user_id',$user->id)->where('seen',0)
            ->orderBy('notifies.id', 'DESC')->get();
        return view('widgets.notify', [
            'config' => $this->config,
            'notifies' => $notifies,
        ]);
    }
}
