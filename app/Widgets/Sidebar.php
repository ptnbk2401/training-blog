<?php

namespace App\Widgets;

use App\Model\Post\PostIndex;
use Arrilot\Widgets\AbstractWidget;

class Sidebar extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $objPost = PostIndex::where('status',1)->inRandomOrder()->limit(6)->get();
        $objPostPopular = PostIndex::where('status',1)->inRandomOrder()->limit(5)->get();
        $objPostRecent = PostIndex::orderBy('id','DESC')->limit(5)->get();
        return view('widgets.sidebar', [
            'config' => $this->config,
            'objPost' => $objPost,
            'objPostPopular' => $objPostPopular,
            'objPostRecent' => $objPostRecent,
        ]);
    }
}
