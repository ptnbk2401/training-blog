<?php

namespace App\Widgets;

use App\Model\Category\Category;
use App\Model\Post\PostIndex;
use Arrilot\Widgets\AbstractWidget;

class Footer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $objItems = PostIndex::where('status',1)->inRandomOrder()->limit(3)->get();
        $objCat = Category::orderBy('sort','DESC')->orderBy('id','DESC')->limit(12)->get();
        return view('widgets.footer', [
            'config' => $this->config,
            'objItems' => $objItems,
            'objCat' => $objCat,
        ]);
    }
}
