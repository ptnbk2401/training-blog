<?php

namespace App\Widgets;

use App\Model\Post\PostIndex;
use Arrilot\Widgets\AbstractWidget;

class TopSliderIndex extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $objPost = PostIndex::where('status',1)->inRandomOrder()->limit(5)->get();
        return view('widgets.top_slider_index', [
            'config' => $this->config,
            'objPost' => $objPost,
        ]);
    }
}
