<?php

namespace App\Policies;

use App\Model\Post\PostIndex;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post index.
     *
     * @param  \App\User  $user
     * @param  \App\PostIndex  $postIndex
     * @return mixed
     */
    public function view(User $user, PostIndex $postIndex)
    {
        return true;
    }

    /**
     * Determine whether the user can create post indices.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the post index.
     *
     * @param  \App\User  $user
     * @param  \App\PostIndex  $postIndex
     * @return mixed
     */
    public function update(User $user, PostIndex $postIndex)
    {
        return  $user->isAdmin($user->id) or $user->id === $postIndex->user_id;
    }

    /**
     * Determine whether the user can delete the post index.
     *
     * @param  \App\User  $user
     * @param  \App\PostIndex  $postIndex
     * @return mixed
     */
    public function delete(User $user, PostIndex $postIndex)
    {
        return  $user->isAdmin($user->id) or $user->id === $postIndex->user_id;
    }

    /**
     * Determine whether the user can restore the post index.
     *
     * @param  \App\User  $user
     * @param  \App\PostIndex  $postIndex
     * @return mixed
     */
    public function restore(User $user, PostIndex $postIndex)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the post index.
     *
     * @param  \App\User  $user
     * @param  \App\PostIndex  $postIndex
     * @return mixed
     */
    public function forceDelete(User $user, PostIndex $postIndex)
    {
        //
    }
}
