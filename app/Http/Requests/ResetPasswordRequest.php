<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required|max:150',
            'email' => 'required|max:150|email',
            'password' => 'required|min:5|confirmed'
        ];
    }

    public function messages()
    {
        return [
            'token.required' => 'Mã code không đúng vui lòng kiểm tra lại email',
            'token.max' => 'Mã code không đúng vui lòng kiểm tra lịa email',
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'Vui long nhập email đúng',
            'password.min' => 'Mật khẩu ít nhất 5 ký tự',
            'password.confirmed' => 'Mật khẩu nhập không khớp',
            'password.required' => 'Mật khẩu không được để rỗng'
        ];
    }
}
