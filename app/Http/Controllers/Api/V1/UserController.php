<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Model\User\GroupIndex;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $objmUser)
    {
        return User::all();
    }

    public function index01()
    {
        $users =  User::paginate(10);
        return $users;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function groups()
    {
        return GroupIndex::select('id','gname')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'name'     => trim($request->name),
            'email'    => $request->email,
            'password'    => bcrypt($request->password),
        ]);
        foreach ($request->groups as $group) {
            $user->groups()
            ->attach(GroupIndex::whereId($group)->first());
        }
        return response([
            'result' => 'success'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $groups = $user->groups;
        $result = [$user,$groups];
        return response([
            'user' => $user,
            'groups' => $groups,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $user = User::find($id);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if(!empty($request->password)) {
           $user->password = bcrypt($request->password);
        }
        $user->save();
        $user->groups()->detach();
        foreach ($request->groups as $group) {
            $user->groups()
            ->attach(GroupIndex::whereId($group)->first());
        }
        return response([
            'result' => 'success'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if(count($user->posts)) {
            return response([
                'result' => 'warning',
            ], 200);
        } else {
            $user->groups()->detach();
            User::whereId($id)->delete();
            return response([
                'result' => 'success',
            ], 200);
        }
    }
}
