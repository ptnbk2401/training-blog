<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use App\Mail\ResetPass;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Socialite;
use Mail;


class AuthController extends Controller
{

    public function postLogin(Request $request){
    	$username = $request->username;
    	$password = $request->password;

    	if (Auth::attempt(['username' => $username, 'password' => $password])) {
            $id_user = Auth::user()->id;
            $objCB = User::getCapbacUser($id_user);
            if($objCB->code == 'khachhang') {
                return redirect()->intended(route('vpublic.core.pcindex.index'));
            }
            return redirect()->intended(route('vadmin.core.index.index'));
        } else {
            $request->session()->flash('msg', 'Sai username hoặc password!');
        	return redirect()->intended(route('auth.auth.login'));
        }
    }
    public function postSenMail(Request $request){
        $mail = $request->email;
        $token = md5(trim($request->email)).str_random(10);
        $user = User::where('email',$mail)->first();

        if(!$user){
            return redirect()->route('public.index');
        }
        $user->reset_password_token = $token;
        $user->save();
        $arItem = [
            'token' => $token
        ];
        try{
           Mail::to( $mail )->send(new ResetPass($arItem));
        }catch (Exception $e){
            throw new MailException('progress.sentMailError');
        }

        return redirect()->route('public.index')->with('msg-al','Đã gửi yêu cầu đổi Password vào Email');
//        return config('const.statusSuccess');
    }
    public function resetPassword($token)
    {
        return view('auth.mail.change_passs', compact('token'));
    }

    public function updatePassword(ResetPasswordRequest $request)
    {
        $code = $request->token;
        $objUser = User::where('reset_password_token', $code)->first();
        if (!is_null($objUser)) {
            if ($objUser->email != trim($request->email)) return redirect()->back()->with('msgrs', 'Email không trùng khớp, vui lòng kiểm tra lại email!');
            $objUser->password = bcrypt(trim($request->password));
            $objUser->reset_password_token = null;
            $objUser->save();
            Auth::login($objUser, true);
            return redirect('/')->with('msg-al', 'Cập nhật mật khẩu thành công!');
        } else {
            return redirect()->back()->with('msgrs', 'Link xác nhận không đúng,Vui lòng kiểm tra email!');
        }
    }


    
}
