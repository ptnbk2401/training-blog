<?php

namespace App\Http\Controllers\Admin;

use App\Message;
use App\Model\Avatar\Avatar;
use App\Model\User\Profile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Pusher\Pusher;

class UserIndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.user.profile',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.user.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = \Auth::user();
        $profile = new Profile();
        $ratings = $profile->getRanting($user->id) ;
        DB::table('notifies')->where('user_id',$user->id)->update(['seen'=>1]);
        return view('admin.user.profile',compact('user','ratings'));
    }


    public function postAddProfile(Request $request, $id )
    {
        $user = User::find($id);
        $user->name = trim($request->name);
        $user->save();

        if(!empty($user->profile)) {
            $profile = $user->profile;
            $profile->gender = $request->gender;
            $profile->education = trim($request->education);
            $profile->location = trim($request->location);
            $profile->notes = trim($request->notes);
            $profile->skills = implode('|',($request->skills));
            $profile->save();
        } else {
            $profile = new Profile();
            $profile->user_id = $id;
            $profile->gender = $request->gender;
            $profile->education = trim($request->education);
            $profile->location = trim($request->location);
            $profile->skills = implode('|',($request->skills));
            $profile->notes = trim($request->notes);
            $profile->save();
        }

       return redirect()->route('profile');
    }

    public function changeAvatar(Request $request)
    {
        $user = \Auth::user();

        $avatar = !empty($user->avatar) ? $user->avatar : new Avatar();
        $avatar->user_id = $user->id;
        if (!empty($request->avatar)) {
            if (Input::hasFile('avatar')) {
                if(!empty($avatar->avatar)){

                    $old_avatar = $avatar->avatar;
                } else {
                    $old_avatar = '';
                }
                $extension = Input::file('avatar')->getClientOriginalExtension();
                $fileName = str_slug($user->name) . '-' . time() . '.' . $extension;
                $request->file('avatar')->move(storage_path('app/public/media/files/users'), $fileName);
                $anh = \App\Http\Utils\FileResize::resizeResultPathFile($fileName, 'users', 88, 88) ;
                $avatar->avatar =  $fileName;
                $avatar->save();
                if(!empty($old_avatar) && !empty($anh) ) {
                    Storage::delete("public/media/files/users/".$old_avatar);
                }
                return $anh;
            }
        } else return 0;

    }

    public function message()
    {
        $messages = Message::orderBy('id','DESC')->limit(10)->get();
        return view('admin.user.message',compact('messages'));
    }

    public function postMessage(Request $request)
    {
        $mess = $request->message;
        $user = \Auth::user();
        $message = $user->messages()->create([
            'message' => trim($mess),
        ]);
        Carbon::setLocale('vi');
        $now     = Carbon::now();
        $dt = Carbon::createFromTimeString( $message->created_at);


        if(!empty($user->avatar)) {
            $hinhanh = $user->avatar->avatar;
            $path = storage_path('app/public/media/files/users/' .$user->avatar->avatar) ;
            if( !empty( $hinhanh ) && file_exists( $path ) ) {
                $avatar = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'users', 40, 40) ;
            } else {
                $avatar = '/templates/core/images/avatar-profile.png';
            }
        }
        else {
            $avatar = '/templates/core/images/avatar-profile.png';
        }
        $data = [
            'user_id' => \Auth::id(),
            'message' => trim($mess),
            'avatar' => $avatar,
            'from_name' => $user->name,
            'time' => $dt->diffForHumans($now),
        ];
        $this->sendPuscher($data);

        if(!empty($user->avatar)) {
            $hinhanh = $user->avatar->avatar;
            $path = storage_path('app/public/media/files/users/' .$user->avatar->avatar) ;
            if( !empty( $hinhanh ) && file_exists( $path ) ) {
                $anh = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'users', 40, 40) ;
            } else {
                $anh = '/templates/core/images/avatar-profile.png';
            }
        }
        else {
            $anh = '/templates/core/images/avatar-profile.png';
        }
        $html = '<div class="direct-chat-msg right" >';
        $html .='<div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">'. \Auth::user()->name .'</span>';
        $html .='<span class="direct-chat-timestamp pull-left">'. $dt->diffForHumans($now) .'</span>';
        $html .='</div>';
        $html .='<img class="direct-chat-img" src="'.$anh.'" alt="message user image">';
        $html .='<div class="direct-chat-text" style="text-align: right; background: #01ad4ec7">'.$mess.'</div></div>';

        return $html;
    }

    function sendPuscher($data){
        $options = array(
            'cluster' => 'mt1',
            'encrypted' => true
        );

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $pusher->trigger('MessageSent', 'chat', $data);
        return 1;
    }





}
