<?php

namespace App\Http\Controllers\Vpublic;

use App\Http\Controllers\Controller;
use App\Model\Category\Category;
use App\Model\Notify\Notify;
use App\Model\Post\PostIndex;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Pusher\Pusher;

class PcIndexController extends Controller
{
    public function __construct(PostIndex $objmPost)
    {
        $this->objmPost = $objmPost;

    }

    public function index()
    {
        return view('vpublic.core.pcindex.index');
    }
    public function detail($cat,$title,$id)
    {
        $catSlug = $cat;
        $post = PostIndex::find($id);
        $expiresAt = now()->addHours(1);
        views($post)->delayInSession($expiresAt)->record();
        return view('vpublic.core.pcsingle.index',compact('post','catSlug'));
    }
    public function category($cat,$id)
    {
        $cat = Category::find($id);
        $objPosts = $cat->posts()->orderBy('id','DESC')->paginate(12);
        return view('vpublic.core.pcblog.index',compact('objPosts','cat'));
    }
    public function search(Request $request)
    {
        $search = $request->c;
        if (!empty($search)) {
            $objPosts = $this->objmPost->getItemsBySearch($search);
            return view('vpublic.core.pcsearch.index',compact('objPosts','search'));
        }
        else {
            return redirect()->back();
        }
    }

    public function postReview(Request $request)
    {
        $post = PostIndex::find($request->id);
        $rank = $post->ratings()->where('user_id', \Auth::id())->orderby('id','DESC')->first();
        Carbon::setLocale('vi');
        $now     = Carbon::now();
        if(!empty($rank)) {
            $dt = Carbon::createFromTimeString($rank->created_at);
            $now = Carbon::now();
            if( $now->diffInMinutes($dt) < 60 ) {
                $request->session()->flash('msg-al', 'Bạn đã đánh giá bài viết này trước đó!');
                $request->session()->push('rating-'.$request->id.'-'.\Auth::id(), '1');
                return redirect()->back();
            }
        }

        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = $request->rate;
        $rating->user_id = \Auth::id();
        $post->ratings()->save($rating);
        ($request->session()->has('rating'))? $request->session()->forget('rating') : $request->session()->push('rating-'.$request->id.'-'.\Auth::id(), '1');
        $request->session()->flash('msg-al', 'Cảm ơn bạn đã đánh giá bài viết!');

        if(!empty(\Auth::user()->avatar)) {
            $hinhanh = \Auth::user()->avatar->avatar;
            $path = storage_path('app/public/media/files/users/' .\Auth::user()->avatar->avatar) ;
            if( !empty( $hinhanh ) && file_exists( $path ) ) {
                $avatar = \App\Http\Utils\FileResize::resizeResultPathFile($hinhanh, 'users', 40, 40) ;
            } else {
                $avatar = '/templates/core/images/avatar-profile.png';
            }
        }
        else {
            $avatar = '/templates/core/images/avatar-profile.png';
        }

        $dt = Carbon::createFromTimeString($rating->created_at);

        if ( $now->diffInHours($dt) <= 23 ){
            $time = $dt->diffForHumans($now) ;
        }
        else{
            $time =  date('d/m/Y',strtotime($dt));
        }
        $data = [
            'content'  =>'Đánh giá bài viết '.$request->rate. ' Sao',
            'avatar' => $avatar,
            'from_user_id' => $rating->user_id,
            'from_name' =>  \Auth::user()->name,
            'user_id' => $post->user->id,
            'time'    => $time
        ];
        $this->sendPuscher($data);
        $notify = new Notify();
        $notify->content  = $data['content'] ;
        $notify->user_id = $data['user_id'] ;
        $notify->from_user_id = $data['from_user_id']  ;
        $notify->seen = 0;
        $notify->save();
        return redirect()->back();

    }

    function sendPuscher($data){
        $options = array(
            'cluster' => 'mt1',
            'encrypted' => true
        );

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $pusher->trigger('Notify', 'send-message-'.$data['user_id'], $data);
        return 1;
    }
    
}
