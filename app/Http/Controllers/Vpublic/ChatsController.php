<?php

namespace App\Http\Controllers\Vpublic;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\MessageSent;

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vpublic.core.chat.index');
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages()
    {
        return Message::with('user')->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $user = \Auth::user();

        $message = $user->messages()->create([
            'message' => $request->input('message')
        ]);
//        dd(broadcast(new MessageSent($user, $message))->toOthers());
        broadcast(new MessageSent($user, $message))->toOthers();

        return ['status' => 'Message Sent!'];
    }
}
