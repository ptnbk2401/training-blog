<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UserTableSeeder::class);
//         $this->call(PostTableSeeder::class);
//         $this->call(CatTableSeeder::class);
//         $this->call(GroupsTableSeeder::class);
//         $this->call(UserGroupTableSeeder::class);
         $this->call(PostCategoryTableSeeder::class);
//         $this->call(UserPostTableSeeder::class);
    }
}

Class CatTableSeeder extends Seeder 
{
	public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'Giải Trí'],
            ['name' => 'Thời Sự'],
            ['name' => 'Thể Thao'],
        ]);
    }
	
}
Class GroupsTableSeeder extends Seeder 
{
    public function run()
    {
        DB::table('groups')->insert([
            ['gname' => 'admin'],
            ['gname' => 'editor'],
            ['gname' => 'user'],
        ]);
    }
    
}
Class UserGroupTableSeeder extends Seeder 
{
    public function run()
    {
        DB::table('user_group')->insert([
            ['user_id' => '1', 'group_id'=>'1'],
            ['user_id' => '2', 'group_id'=>'2'],
            ['user_id' => '3', 'group_id'=>'3'],
        ]);
    }
    
}
Class PostCategoryTableSeeder extends Seeder 
{
    public function run()
    {
        DB::table('post_category')->insert([
            ['post_id' => '1', 'cat_id'=>'1'],
            ['post_id' => '2', 'cat_id'=>'1'],
            ['post_id' => '3', 'cat_id'=>'1'],
            ['post_id' => '4', 'cat_id'=>'1'],
            ['post_id' => '5', 'cat_id'=>'1'],
            ['post_id' => '6', 'cat_id'=>'2'],
            ['post_id' => '7', 'cat_id'=>'2'],
            ['post_id' => '8', 'cat_id'=>'2'],
            ['post_id' => '9', 'cat_id'=>'2'],
            ['post_id' => '10', 'cat_id'=>'2'],
            ['post_id' => '11', 'cat_id'=>'2'],
            ['post_id' => '12', 'cat_id'=>'3'],
            ['post_id' => '13', 'cat_id'=>'3'],
            ['post_id' => '14', 'cat_id'=>'3'],
            ['post_id' => '15', 'cat_id'=>'3'],
            ['post_id' => '16', 'cat_id'=>'3'],
            ['post_id' => '17', 'cat_id'=>'3'],
            ['post_id' => '18', 'cat_id'=>'3'],
            ['post_id' => '19', 'cat_id'=>'3'],
            ['post_id' => '20', 'cat_id'=>'3'],
            ['post_id' => '21', 'cat_id'=>'2'],
            ['post_id' => '22', 'cat_id'=>'3'],
            ['post_id' => '23', 'cat_id'=>'2'],
            ['post_id' => '24', 'cat_id'=>'2'],
            ['post_id' => '25', 'cat_id'=>'2'],
            ['post_id' => '26', 'cat_id'=>'3'],
            ['post_id' => '27', 'cat_id'=>'3'],
            ['post_id' => '28', 'cat_id'=>'3'],
            ['post_id' => '29', 'cat_id'=>'3'],
            ['post_id' => '30', 'cat_id'=>'3'],
            ['post_id' => '31', 'cat_id'=>'1'],
            ['post_id' => '32', 'cat_id'=>'1'],
            ['post_id' => '33', 'cat_id'=>'1'],
            ['post_id' => '34', 'cat_id'=>'1'],
            ['post_id' => '35', 'cat_id'=>'1'],
            ['post_id' => '36', 'cat_id'=>'2'],
            ['post_id' => '37', 'cat_id'=>'2'],
            ['post_id' => '38', 'cat_id'=>'2'],
            ['post_id' => '39', 'cat_id'=>'2'],
            ['post_id' => '40', 'cat_id'=>'2'],
            ['post_id' => '41', 'cat_id'=>'1'],
            ['post_id' => '42', 'cat_id'=>'1'],
            ['post_id' => '43', 'cat_id'=>'1'],
            ['post_id' => '44', 'cat_id'=>'1'],
            ['post_id' => '45', 'cat_id'=>'1'],
            ['post_id' => '46', 'cat_id'=>'2'],
            ['post_id' => '47', 'cat_id'=>'2'],
            ['post_id' => '48', 'cat_id'=>'2'],
            ['post_id' => '49', 'cat_id'=>'2'],
            ['post_id' => '50', 'cat_id'=>'2'],

        ]);
    }
    
}
Class UserPostTableSeeder extends Seeder 
{
    public function run()
    {
        DB::table('user_post')->insert([
            ['post_id' => '1', 'user_id'=>'1'],
            ['post_id' => '2', 'user_id'=>'1'],
            ['post_id' => '3', 'user_id'=>'1'],
            ['post_id' => '4', 'user_id'=>'1'],
            ['post_id' => '5', 'user_id'=>'1'],
            ['post_id' => '6', 'user_id'=>'2'],
            ['post_id' => '7', 'user_id'=>'2'],
            ['post_id' => '8', 'user_id'=>'2'],
            ['post_id' => '9', 'user_id'=>'2'],
            ['post_id' => '10', 'user_id'=>'2'],
            ['post_id' => '11', 'user_id'=>'2'],
            ['post_id' => '12', 'user_id'=>'3'],
            ['post_id' => '13', 'user_id'=>'3'],
            ['post_id' => '14', 'user_id'=>'3'],
            ['post_id' => '15', 'user_id'=>'3'],
            ['post_id' => '16', 'user_id'=>'3'],
            ['post_id' => '17', 'user_id'=>'3'],
            ['post_id' => '18', 'user_id'=>'3'],
            ['post_id' => '19', 'user_id'=>'3'],
            ['post_id' => '20', 'user_id'=>'3'],
            ['post_id' => '1', 'user_id'=>'2'],
            ['post_id' => '2', 'user_id'=>'3'],
            ['post_id' => '3', 'user_id'=>'2'],
            ['post_id' => '4', 'user_id'=>'2'],
            ['post_id' => '5', 'user_id'=>'2'],
            ['post_id' => '8', 'user_id'=>'3'],
            ['post_id' => '9', 'user_id'=>'3'],
            ['post_id' => '10', 'user_id'=>'3'],
            ['post_id' => '11', 'user_id'=>'3'],
        ]);
    }
    
}