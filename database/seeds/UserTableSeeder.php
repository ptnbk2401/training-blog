<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Model\User\GroupIndex;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_admin 	= GroupIndex::where('gname', 'admin')->first();
		$role_editor  	= GroupIndex::where('gname', 'editor')->first();
		$role_user 		= GroupIndex::where('gname', 'user')->first();

		$admin = new User();
		$admin->name = 'Admin Super';
		$admin->email = 'admin01@gmail.com';
		$admin->password = bcrypt('123123');
		$admin->save();
		$admin->groups()->attach($role_admin);

		$editor = new User();
		$editor->name = 'Ngọc Trinh';
		$editor->email = 'editor01@gmail.com';
		$editor->password = bcrypt('123123');
		$editor->save();
		$editor->groups()->attach($role_editor);

		$user = new User();
		$user->name = 'Sơn Tùng';
		$user->email = 'user01@gmail.com';
		$user->password = bcrypt('123123');
		$user->save();
		$user->groups()->attach($role_user);
    }
}
